﻿Public Class AgregarCliente

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        Dim cli As New DatosClientes

        If Trim(txtDocumento.Text) <> "" And Trim(txtNombre.Text) <> "" And Trim(txtApellido.Text) <> "" Then

            Try
                cli.Documento_ = Trim(txtDocumento.Text)
                cli.Nombre_ = txtNombre.Text
                cli.Apellido_ = txtApellido.Text

                If Trim(txtDomicilio.Text) <> "" Then
                    cli.Direccion_ = txtDomicilio.Text
                Else
                    cli.Direccion_ = "-"
                End If

                If Trim(txtCelular.Text) <> "" Then
                    cli.Telefono_ = txtCelular.Text
                Else
                    cli.Telefono_ = "-"
                End If

                If Trim(txtEmail.Text) <> "" Then
                    cli.Email_ = txtEmail.Text
                Else
                    cli.Email_ = "-"
                End If

                cli.NuevoCliente()
                MsgBox("Se agregó con éxito el Cliente.", MsgBoxStyle.Information, Title:="")

                Me.DialogResult = DialogResult.OK

            Catch ex As Exception
                MessageBox.Show("Ocurrio un problema al cargar el Cliente. Detalle:" & ex.Message.ToString)
            End Try

        Else
            MessageBox.Show("Complete los datos obligatorios: documento, nombre y apellido.")
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Dispose()
    End Sub
End Class