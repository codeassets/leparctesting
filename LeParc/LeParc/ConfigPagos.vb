﻿Imports System.Data.SqlClient

Public Class ConfigPagos

    Private strConn As String = "Data Source=190.105.225.136\MSSQLSERVER2012;Initial Catalog=CodeAssets;Persist Security Info=True;User ID=codeassets;Password=%p95O6qg"
    Private sqlCon As SqlConnection

    Sub FormatearTabla()
        dgvPagos.Columns("id").Visible = False
        dgvPagos.Columns("id_sistema").Visible = False
        dgvPagos.Columns("fecha_cuota").HeaderText = "Fecha"
        dgvPagos.Columns("fecha_vencimiento").HeaderText = "Vencimiento"
        dgvPagos.Columns("monto").HeaderText = "Monto"
        dgvPagos.Columns("monto_vencimiento").HeaderText = "Monto vencimiento"
        dgvPagos.Columns("estado").HeaderText = "Estado"
        dgvPagos.Columns("fecha_pago").HeaderText = "Fecha de pago"

        For Each row As DataGridViewRow In dgvPagos.Rows
            If row.Cells("estado").Value = "Pagada" Then
                row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
            ElseIf row.Cells("estado").Value = "Impaga" Then
                row.DefaultCellStyle.BackColor = Color.Salmon
            End If
        Next

        dgvPagos.ClearSelection()
    End Sub



    Private Sub cboxAños_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxAños.SelectedIndexChanged
        sqlCon = New SqlConnection(strConn)

        '' Controlar cuota
        Using (sqlCon)

            Dim dt As New DataTable
            Dim da As New SqlDataAdapter("VerCuotasPorSistema", strConn)
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.SelectCommand.Parameters.AddWithValue("id_sistema", "2")
            da.SelectCommand.Parameters.AddWithValue("anio", cboxAños.SelectedItem.ToString)
            da.Fill(dt)

            dgvPagos.DataSource = dt

        End Using

        FormatearTabla()
    End Sub

    Private Sub dgvPagos_Sorted(sender As Object, e As EventArgs) Handles dgvPagos.Sorted
        FormatearTabla()
    End Sub

    Private Sub ConfigPagos_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        For i As Integer = 0 To 10
            cboxAños.Items.Add(Date.Now.Year - i)

        Next

        cboxAños.SelectedIndex = 0

        sqlCon = New SqlConnection(strConn)

        '' Controlar cuota
        Using (sqlCon)

            Dim dt As New DataTable
            Dim da As New SqlDataAdapter("VerCuotasPorSistema", strConn)
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.SelectCommand.Parameters.AddWithValue("id_sistema", "2")
            da.SelectCommand.Parameters.AddWithValue("anio", cboxAños.SelectedItem.ToString)
            da.Fill(dt)

            dgvPagos.DataSource = dt

        End Using

        FormatearTabla()
    End Sub
End Class