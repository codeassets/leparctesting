﻿Public Class Clientes
    Dim opcion As Integer

    Private Sub CaptureImage()

        Dim data As IDataObject
        Dim bmap As Image

        '---copy the image to the clipboard---
        SendMessage(hHwnd, WM_CAP_EDIT_COPY, 0, 0)

        '---retrieve the image from clipboard and convert it to the bitmap format
        data = Clipboard.GetDataObject()
        If data.GetDataPresent(GetType(System.Drawing.Bitmap)) Then
            bmap =
               CType(data.GetData(GetType(System.Drawing.Bitmap)),
               Image)
            PictureBox1.Image = bmap
            StopPreviewWindow()
        End If

    End Sub

    Private Sub StopPreviewWindow()
        SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, iDevice, 0)
        DestroyWindow(hHwnd)
    End Sub

    Private Sub Llenar_Cliente()
        Dim ind As New DatosClientes
        dgvClientes.DataSource = ind.ObtenerTodo()
        dgvClientes.Columns("IdCliente").Visible = False
        dgvClientes.Columns("Activo").Visible = False
        dgvClientes.ClearSelection()
    End Sub

    Private Sub Limpiar_Campos()
        For Each control As Control In TLP_datos.Controls
            txtNombre.Text = ""
            txtApellido.Text = ""
            txtDocumento.Text = ""
            txtDomicilio.Text = ""
            txtCelular.Text = ""
            txtDocumento.Text = ""
            txtEmail.Text = ""
        Next
    End Sub

    Private Sub bttAgregarCliente_Click(sender As Object, e As EventArgs) Handles bttAgregarCliente.Click

        Limpiar_Campos()
        TLP_datos.Enabled = True
        txtDocumento.Focus()
        bttAgregarCliente.Enabled = False
        bttEditarCliente.Enabled = False
        bttEliminar.Enabled = False
        bttGuardarCliente.Enabled = True
        bttCancelar.Enabled = True
        dgvClientes.Enabled = False
        bttFoto.Enabled = True
        bttBorrarFoto.Enabled = True
        PictureBox1.Enabled = True

        opcion = 1

        'Iniciar Cámara
        On Error Resume Next

        Dim iHeight As Integer = PictureBox1.Height
        Dim iWidth As Integer = PictureBox1.Width

        ' Open Preview window in picturebox
        hHwnd = capCreateCaptureWindowA(iDevice, WS_VISIBLE Or WS_CHILD, 0, 0, 640,
            480, PictureBox1.Handle.ToInt32, 0)

        ' Connect to device
        If SendMessage(hHwnd, WM_CAP_DRIVER_CONNECT, iDevice, 0) Then

            'Set the preview scale
            SendMessage(hHwnd, WM_CAP_SET_SCALE, True, 0)

            'Set the preview rate in milliseconds
            SendMessage(hHwnd, WM_CAP_SET_PREVIEWRATE, 66, 0)

            'Start previewing the image from the camera
            SendMessage(hHwnd, WM_CAP_SET_PREVIEW, True, 0)

            ' Resize window to fit in picturebox
            SetWindowPos(hHwnd, HWND_BOTTOM, 0, 0, PictureBox1.Width, PictureBox1.Height,
                    SWP_NOMOVE Or SWP_NOZORDER)

        Else
            ' Error connecting to device close window
            DestroyWindow(hHwnd)

        End If

    End Sub

    Private Sub bttEditarCliente_Click(sender As Object, e As EventArgs) Handles bttEditarCliente.Click
        opcion = 2
        TLP_datos.Enabled = True
        bttAgregarCliente.Enabled = False
        bttEliminar.Enabled = False
        bttEditarCliente.Enabled = False
        bttGuardarCliente.Enabled = True
        bttCancelar.Enabled = True
        bttBorrarFoto.Enabled = True
        dgvClientes.Enabled = False
    End Sub



    Private Sub bttGuardarCliente_Click(sender As Object, e As EventArgs) Handles bttGuardarCliente.Click
        Dim cli As New DatosClientes

        'Dim Resultado_Validar As Boolean = Validar_Campos()
        'If Resultado_Validar = True Then

        If Trim(txtDocumento.Text) <> "" And Trim(txtNombre.Text) <> "" And Trim(txtApellido.Text) <> "" Then

            If opcion = 1 Then 'AGREGAR
                Try
                    cli.Documento_ = Trim(txtDocumento.Text)
                    cli.Nombre_ = txtNombre.Text
                    cli.Apellido_ = txtApellido.Text

                    If Trim(txtDomicilio.Text) <> "" Then
                        cli.Direccion_ = txtDomicilio.Text
                    Else
                        cli.Direccion_ = "-"
                    End If

                    If Trim(txtCelular.Text) <> "" Then
                        cli.Telefono_ = txtCelular.Text
                    Else
                        cli.Telefono_ = "-"
                    End If

                    If Trim(txtEmail.Text) <> "" Then
                        cli.Email_ = txtEmail.Text
                    Else
                        cli.Email_ = "-"
                    End If

                    If Not PictureBox1.Image Is Nothing Then
                        Dim im As New Bitmap(PictureBox1.Image)
                        PictureBox1.Image = Nothing

                        Dim rutaPrincipal As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
                        rutaPrincipal = rutaPrincipal.Remove(0, 6)
                        If (Not System.IO.Directory.Exists(rutaPrincipal + "\fotos\")) Then
                            System.IO.Directory.CreateDirectory(rutaPrincipal + "\fotos\")
                        End If

                        im.Save(rutaPrincipal + "\fotos\" + cli.Documento_.ToString + cli.Nombre_ + cli.Apellido_ + ".jpeg",
                            System.Drawing.Imaging.ImageFormat.Jpeg)

                        cli.DirImagen_ = rutaPrincipal + "\fotos\" + cli.Documento_.ToString + cli.Nombre_ + cli.Apellido_ + ".jpeg"
                    Else
                        cli.DirImagen_ = ""
                    End If

                    cli.InsertarCliente()
                    MsgBox("Se agregó con éxito el Cliente.", MsgBoxStyle.Information, Title:="")
                    dgvClientes.Enabled = True
                    bttFoto.Enabled = False
                    Llenar_Cliente()
                    bttCancelar.PerformClick()
                    txtBuscar.Clear()


                Catch ex As Exception
                    MessageBox.Show("Ocurrio un problema al cargar el Cliente. Detalle:" & ex.Message.ToString)
                End Try
            ElseIf opcion = 2 Then 'MODIFICAR
                Try
                    cli.IdCliente_ = dgvClientes.CurrentRow.Cells("IdCliente").Value
                    cli.Documento_ = Trim(txtDocumento.Text)
                    cli.Nombre_ = txtNombre.Text
                    cli.Apellido_ = txtApellido.Text

                    If Trim(txtDomicilio.Text) <> "" Then
                        cli.Direccion_ = txtDomicilio.Text
                    Else
                        cli.Direccion_ = "-"
                    End If

                    If Trim(txtCelular.Text) <> "" Then
                        cli.Telefono_ = txtCelular.Text
                    Else
                        cli.Telefono_ = "-"
                    End If

                    If Trim(txtEmail.Text) <> "" Then
                        cli.Email_ = txtEmail.Text
                    Else
                        cli.Email_ = "-"
                    End If

                    If Not PictureBox1.Image Is Nothing Then
                        Dim im As New Bitmap(PictureBox1.Image)
                        PictureBox1.Image = Nothing

                        Dim rutaPrincipal As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
                        rutaPrincipal = rutaPrincipal.Remove(0, 6)
                        If (Not System.IO.Directory.Exists(rutaPrincipal + "\fotos\")) Then
                            System.IO.Directory.CreateDirectory(rutaPrincipal + "\fotos\")
                        End If

                        im.Save(rutaPrincipal + "\fotos\" + cli.Documento_.ToString + cli.Nombre_ + cli.Apellido_ + ".jpeg",
                            System.Drawing.Imaging.ImageFormat.Jpeg)

                        cli.DirImagen_ = rutaPrincipal + "\fotos\" + cli.Documento_.ToString + cli.Nombre_ + cli.Apellido_ + ".jpeg"
                    Else
                        cli.DirImagen_ = ""
                    End If

                    cli.ModificarCliente()
                    MsgBox("Se modificó el cliente con éxito.", MsgBoxStyle.Information, Title:="")
                    dgvClientes.Enabled = True
                    bttFoto.Enabled = False
                    Llenar_Cliente()
                    bttCancelar.PerformClick()
                    txtBuscar.Clear()
                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el cliente. Detalle:" & ex.Message.ToString)
                End Try
            End If
        Else
            MessageBox.Show("Complete todos los datos.")
        End If
    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        TLP_datos.Enabled = False

        bttAgregarCliente.Enabled = True
        bttCancelar.Enabled = False
        bttEliminar.Enabled = False

        dgvClientes.Enabled = True
        dgvClientes.ClearSelection()

        Limpiar_Campos()

        PictureBox1.Image = Nothing

        ' Disconnect from device
        SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, iDevice, 0)

        ' close window
        DestroyWindow(hHwnd)

    End Sub



    Private Sub Clientes_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        If FormPrincipal.Width > 1300 Then
            bttAgregarCliente.Text = "Agregar"
            bttEditarCliente.Text = "Editar"
            bttEliminar.Text = "Eliminar"
            bttAgregarCliente.Size = New Size(121, 44)
            bttEditarCliente.Size = New Size(121, 44)
            bttEliminar.Size = New Size(121, 44)
            bttFoto.Text = "Capturar"
            bttBorrarFoto.Text = "Borrar"
        Else
            bttAgregarCliente.Text = ""
            bttEditarCliente.Text = ""
            bttEliminar.Text = ""
            bttFoto.Text = ""
            bttBorrarFoto.Text = ""
        End If
    End Sub

    Private Sub Clientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Llenar_Cliente()
            dgvClientes.Columns("DirImagen").Visible = False
            dgvClientes.Columns("Nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
            dgvClientes.Columns("Apellido").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
            dgvClientes.Columns("Documento").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema al cargar la lista de Cliente. Detalle:" & ex.Message.ToString)
        End Try
    End Sub

    Private Sub txtDNI_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDocumento.KeyPress
        If e.KeyChar <> ChrW(Keys.Back) Then
            If Char.IsNumber(e.KeyChar) Then
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim cli As New DatosClientes
        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este cliente?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                cli.IdCliente_ = dgvClientes.CurrentRow.Cells("IdCliente").Value
                cli.EliminarCliente()
                MsgBox("Se eliminó el cliente con éxito.", MsgBoxStyle.Information, Title:="")
                dgvClientes.Enabled = True
                Llenar_Cliente()
                bttCancelar.PerformClick()
                txtBuscar.Clear()
                bttEliminar.Enabled = False
                Limpiar_Campos()

                PictureBox1.Image = Nothing

                ' Disconnect from device
                SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, iDevice, 0)

                ' close window
                DestroyWindow(hHwnd)
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el cliente. Detalle:" & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttFoto_Click(sender As Object, e As EventArgs) Handles bttFoto.Click
        Try
            CaptureImage()
            bttGuardarCliente.Enabled = True
        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al tomar la foto. Detalle:" & ex.Message.ToString)
        End Try
    End Sub

    Private Sub bttBorrarFoto_Click(sender As Object, e As EventArgs) Handles bttBorrarFoto.Click
        If opcion = 2 Then
            If System.IO.File.Exists(dgvClientes.CurrentRow.Cells("DirImagen").Value) Then
                PictureBox1.Image.Dispose()
                My.Computer.FileSystem.DeleteFile(dgvClientes.CurrentRow.Cells("DirImagen").Value)
            End If
        End If
        bttFoto.Enabled = True
        bttGuardarCliente.Enabled = False

        'Iniciar Cámara
        On Error Resume Next

        Dim iHeight As Integer = PictureBox1.Height
        Dim iWidth As Integer = PictureBox1.Width

        ' Open Preview window in picturebox
        hHwnd = capCreateCaptureWindowA(iDevice, WS_VISIBLE Or WS_CHILD, 0, 0, 640,
            480, PictureBox1.Handle.ToInt32, 0)

        ' Connect to device
        If SendMessage(hHwnd, WM_CAP_DRIVER_CONNECT, iDevice, 0) Then

            'Set the preview scale
            SendMessage(hHwnd, WM_CAP_SET_SCALE, True, 0)

            'Set the preview rate in milliseconds
            SendMessage(hHwnd, WM_CAP_SET_PREVIEWRATE, 66, 0)

            'Start previewing the image from the camera
            SendMessage(hHwnd, WM_CAP_SET_PREVIEW, True, 0)

            ' Resize window to fit in picturebox
            SetWindowPos(hHwnd, HWND_BOTTOM, 0, 0, PictureBox1.Width, PictureBox1.Height,
                    SWP_NOMOVE Or SWP_NOZORDER)

        Else
            ' Error connecting to device close window
            DestroyWindow(hHwnd)

        End If

    End Sub

    Private Sub Clientes_Leave(sender As Object, e As EventArgs) Handles MyBase.Leave

        On Error Resume Next

        ' Disconnect from device
        SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, iDevice, 0)

        ' close window
        DestroyWindow(hHwnd)

    End Sub

    Private Sub dgvClientes_SelectionChanged(sender As Object, e As EventArgs) Handles dgvClientes.SelectionChanged

        Try
            If dgvClientes.SelectedRows.Count <> 0 Then
                bttEditarCliente.Enabled = True
                bttEliminar.Enabled = True

                txtDocumento.Text = dgvClientes.CurrentRow.Cells("Documento").Value
                txtApellido.Text = dgvClientes.CurrentRow.Cells("Apellido").Value
                txtNombre.Text = dgvClientes.CurrentRow.Cells("Nombre").Value
                txtDomicilio.Text = dgvClientes.CurrentRow.Cells("Direccion").Value
                txtCelular.Text = dgvClientes.CurrentRow.Cells("Telefono").Value
                txtEmail.Text = dgvClientes.CurrentRow.Cells("Email").Value
                If System.IO.File.Exists(dgvClientes.CurrentRow.Cells("DirImagen").Value) Then
                    Try
                        PictureBox1.Image = Image.FromFile(dgvClientes.CurrentRow.Cells("DirImagen").Value)
                    Catch ex As Exception

                    End Try

                End If
            Else
                bttEditarCliente.Enabled = False
                bttEliminar.Enabled = False
                Limpiar_Campos()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtBuscarCliente_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            Dim cli As New DatosClientes
            Dim dt As New DataTable
            Try
                dgvClientes.DataSource = cli.buscar(dt, txtBuscar.Text)
                dgvClientes.Columns("IdCliente").Visible = False
                dgvClientes.Columns("Activo").Visible = False
                dgvClientes.Columns("DirImagen").Visible = False
                dgvClientes.Columns("Nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                dgvClientes.Columns("Apellido").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                dgvClientes.Columns("Documento").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                dgvClientes.ClearSelection()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al cargar la lista de clientes. Detalle:" & ex.Message.ToString)
            End Try
        End If
    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim cli As New DatosClientes
            Dim dt As New DataTable
            Try
                dgvClientes.DataSource = cli.buscar(dt, txtBuscar.Text)
                dgvClientes.Columns("IdCliente").Visible = False
                dgvClientes.Columns("Activo").Visible = False
                dgvClientes.Columns("DirImagen").Visible = False
                dgvClientes.Columns("Nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                dgvClientes.Columns("Apellido").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                dgvClientes.Columns("Documento").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                dgvClientes.ClearSelection()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al cargar la lista de clientes. Detalle:" & ex.Message.ToString)
            End Try
        End If
    End Sub

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave
        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If
    End Sub

    Private Sub Label11_Click(sender As Object, e As EventArgs) Handles Label11.Click

    End Sub
End Class