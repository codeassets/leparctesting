﻿Imports System.Data.SqlClient
Public Class DatosFacturas
#Region "get set"
    Private IdFactura As Int64
    Public Property _IdFactura() As Int64
        Get
            Return IdFactura
        End Get
        Set(ByVal value As Int64)
            IdFactura = value
        End Set
    End Property

    Private IdVenta As Int64
    Public Property _IdVenta() As Int64
        Get
            Return IdVenta
        End Get
        Set(ByVal value As Int64)
            IdVenta = value
        End Set
    End Property

    Private PtoVta As String
    Public Property _PtoVta() As String
        Get
            Return PtoVta
        End Get
        Set(ByVal value As String)
            PtoVta = value
        End Set
    End Property

    Private CbteTipo As Int64
    Public Property _CbteTipo() As Int64
        Get
            Return CbteTipo
        End Get
        Set(ByVal value As Int64)
            CbteTipo = value
        End Set
    End Property

    Private CbteTipo_desc As String
    Public Property _CbteTipo_desc() As String
        Get
            Return CbteTipo_desc
        End Get
        Set(ByVal value As String)
            CbteTipo_desc = value
        End Set
    End Property

    Private Concepto As Int64
    Public Property _Concepto() As Int64
        Get
            Return Concepto
        End Get
        Set(ByVal value As Int64)
            Concepto = value
        End Set
    End Property

    Private Concepto_desc As String
    Public Property _Concepto_desc() As String
        Get
            Return Concepto_desc
        End Get
        Set(ByVal value As String)
            Concepto_desc = value
        End Set
    End Property

    Private DocTipo As Int64
    Public Property _DocTipo() As Int64
        Get
            Return DocTipo
        End Get
        Set(ByVal value As Int64)
            DocTipo = value
        End Set
    End Property

    Private DocTipo_desc As String
    Public Property _DocTipo_desc() As String
        Get
            Return DocTipo_desc
        End Get
        Set(ByVal value As String)
            DocTipo_desc = value
        End Set
    End Property

    Private DocNro As Int64
    Public Property _DocNro() As Int64
        Get
            Return DocNro
        End Get
        Set(ByVal value As Int64)
            DocNro = value
        End Set
    End Property

    Private CbteDesde As Nullable(Of Int64)
    Public Property _CbeteDesde() As Nullable(Of Int64)
        Get
            Return CbteDesde
        End Get
        Set(ByVal value As Nullable(Of Int64))
            CbteDesde = value
        End Set
    End Property

    Private CbteHasta As Nullable(Of Int64)
    Public Property _CbteHasta() As Nullable(Of Int64)
        Get
            Return CbteHasta
        End Get
        Set(ByVal value As Nullable(Of Int64))
            CbteHasta = value
        End Set
    End Property

    Private CbteFch As DateTime
    Public Property _CbteFch() As DateTime
        Get
            Return CbteFch
        End Get
        Set(ByVal value As DateTime)
            CbteFch = value
        End Set
    End Property

    Private ImpTotal As Double
    Public Property _ImpTotal() As Double
        Get
            Return ImpTotal
        End Get
        Set(ByVal value As Double)
            ImpTotal = value
        End Set
    End Property

    Private ImpTotConc As Double
    Public Property _ImpTotConc() As Double
        Get
            Return ImpTotConc
        End Get
        Set(ByVal value As Double)
            ImpTotConc = value
        End Set
    End Property

    Private ImpNeto As Double
    Public Property _ImpNeto() As Double
        Get
            Return ImpNeto
        End Get
        Set(ByVal value As Double)
            ImpNeto = value
        End Set
    End Property

    Private ImpOpEx As Double
    Public Property _ImpOpEx() As Double
        Get
            Return ImpOpEx
        End Get
        Set(ByVal value As Double)
            ImpOpEx = value
        End Set
    End Property

    Private ImpTrib As Double
    Public Property _ImpTrib() As Double
        Get
            Return ImpTrib
        End Get
        Set(ByVal value As Double)
            ImpTrib = value
        End Set
    End Property

    Private ImpIVA As Double
    Public Property _ImpIVA() As Double
        Get
            Return ImpIVA
        End Get
        Set(ByVal value As Double)
            ImpIVA = value
        End Set
    End Property

    Private MonId As String
    Public Property _MonId() As String
        Get
            Return MonId
        End Get
        Set(ByVal value As String)
            MonId = value
        End Set
    End Property

    Private MonCotiz As Int64
    Public Property _MonCotiz() As Int64
        Get
            Return MonCotiz
        End Get
        Set(ByVal value As Int64)
            MonCotiz = value
        End Set
    End Property

    Private AlicIva_Id As Int64
    Public Property _AlicIva_Id() As Int64
        Get
            Return AlicIva_Id
        End Get
        Set(ByVal value As Int64)
            AlicIva_Id = value
        End Set
    End Property

    Private AlicIva_BaseImp As Double
    Public Property _AlicIva_BaseImp() As Double
        Get
            Return AlicIva_BaseImp
        End Get
        Set(ByVal value As Double)
            AlicIva_BaseImp = value
        End Set
    End Property

    Private AlicIva_Importe As Double
    Public Property _AlicIva_Importe() As Double
        Get
            Return AlicIva_Importe
        End Get
        Set(ByVal value As Double)
            AlicIva_Importe = value
        End Set
    End Property

    Private Estado As String
    Public Property _Estado() As String
        Get
            Return Estado
        End Get
        Set(ByVal value As String)
            Estado = value
        End Set
    End Property


    Private TipoPago As String
    Public Property _TipoPago() As String
        Get
            Return TipoPago
        End Get
        Set(ByVal value As String)
            TipoPago = value
        End Set
    End Property

    Private CAE As String
    Public Property _CAE() As String
        Get
            Return CAE
        End Get
        Set(ByVal value As String)
            CAE = value
        End Set
    End Property

    Private CAE_vto As String
    Public Property _CAE_vto() As String
        Get
            Return CAE_vto
        End Get
        Set(ByVal value As String)
            CAE_vto = value
        End Set
    End Property

    Private Mensaje As String
    Public Property _Mensaje() As String
        Get
            Return Mensaje
        End Get
        Set(ByVal value As String)
            Mensaje = value
        End Set
    End Property

    'Detalle Facturas
    Private IdDetalle As Int64
    Public Property _IdDetalle() As Int64
        Get
            Return IdDetalle
        End Get
        Set(ByVal value As Int64)
            IdDetalle = value
        End Set
    End Property

    Private IdProducto As Int64
    Public Property _IdProducto() As Int64
        Get
            Return IdProducto
        End Get
        Set(ByVal value As Int64)
            IdProducto = value
        End Set
    End Property
    Private Descripcion As String
    Public Property _Descripcion() As String
        Get
            Return Descripcion
        End Get
        Set(ByVal value As String)
            Descripcion = value
        End Set
    End Property
    Private Marca As String
    Public Property _Marca() As String
        Get
            Return Marca
        End Get
        Set(ByVal value As String)
            Marca = value
        End Set
    End Property
    Private Color As String
    Public Property _Color() As String
        Get
            Return Color
        End Get
        Set(ByVal value As String)
            Color = value
        End Set
    End Property
    Private Talle As String
    Public Property _Talle() As String
        Get
            Return Talle
        End Get
        Set(ByVal value As String)
            Talle = value
        End Set
    End Property
    Private Cantidad As Int64
    Public Property _Cantidad() As Int64
        Get
            Return Cantidad
        End Get
        Set(ByVal value As Int64)
            Cantidad = value
        End Set
    End Property
    Private PrecioUnitario As Double
    Public Property _PrecioUnitario() As Double
        Get
            Return PrecioUnitario
        End Get
        Set(ByVal value As Double)
            PrecioUnitario = value
        End Set
    End Property
    Private Subtotal As Double
    Public Property _Subtotal() As Double
        Get
            Return Subtotal
        End Get
        Set(ByVal value As Double)
            Subtotal = value
        End Set
    End Property

    Private FechaDesde As Date
    Public Property _FechaDesde() As Date
        Get
            Return FechaDesde
        End Get
        Set(ByVal value As Date)
            FechaDesde = value
        End Set
    End Property

    Private FechaHasta As Date
    Public Property _FechaHasta() As Date
        Get
            Return FechaHasta
        End Get
        Set(ByVal value As Date)
            FechaHasta = value
        End Set
    End Property

#End Region
    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Function InsertarFactura()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Comando.Parameters.AddWithValue("@IdVenta", IdVenta)
        Comando.Parameters.AddWithValue("@PtoVta", PtoVta)
        Comando.Parameters.AddWithValue("@CbteTipo", CbteTipo)
        Comando.Parameters.AddWithValue("@CbteTipo_desc", CbteTipo_desc)
        Comando.Parameters.AddWithValue("@Concepto", Concepto)
        Comando.Parameters.AddWithValue("@Concepto_desc", Concepto_desc)
        Comando.Parameters.AddWithValue("@DocTipo", DocTipo)
        Comando.Parameters.AddWithValue("@DocTipo_desc", DocTipo_desc)
        Comando.Parameters.AddWithValue("@DocNro", DocNro)
        Comando.Parameters.AddWithValue("@CbteDesde", If(CbteDesde, DBNull.Value))
        Comando.Parameters.AddWithValue("@CbteHasta", If(CbteHasta, DBNull.Value))
        Comando.Parameters.AddWithValue("@CbteFch", CbteFch)
        Comando.Parameters.AddWithValue("@ImpTotal", ImpTotal)
        Comando.Parameters.AddWithValue("@ImpTotConc", ImpTotConc)
        Comando.Parameters.AddWithValue("@ImpNeto", ImpNeto)
        Comando.Parameters.AddWithValue("@ImpOpEx", ImpOpEx)
        Comando.Parameters.AddWithValue("@ImpTrib", ImpTrib)
        Comando.Parameters.AddWithValue("@ImpIVA", ImpIVA)
        Comando.Parameters.AddWithValue("@MonId", MonId)
        Comando.Parameters.AddWithValue("@MonCotiz", MonCotiz)
        Comando.Parameters.AddWithValue("@AlicIva_Id", AlicIva_Id)
        Comando.Parameters.AddWithValue("@AlicIva_BaseImp", AlicIva_BaseImp)
        Comando.Parameters.AddWithValue("@AlicIva_Importe", AlicIva_Importe)
        Comando.Parameters.AddWithValue("@Estado", Estado)
        Comando.Parameters.AddWithValue("@TipoPago", TipoPago)
        Comando.Parameters.AddWithValue("@CAE", If(CAE, DBNull.Value))
        Comando.Parameters.AddWithValue("@CAE_vto", If(CAE_vto, DBNull.Value))
        Comando.Parameters.AddWithValue("@Mensaje", If(Mensaje, DBNull.Value))

        Comando.CommandText = "INSERT INTO Facturas(IdVenta, PtoVta, CbteTipo, CbteTipo_desc, Concepto, Concepto_desc,DocTipo, DocTipo_desc, DocNro, CbteDesde, CbteHasta, CbteFch, ImpTotal, ImpTotConc,ImpNeto, ImpOpEx, ImpTrib, ImpIVA, MonId, MonCotiz, AlicIva_Id, AlicIva_BaseImp, AlicIva_Importe, Estado, TipoPago, CAE, CAE_vto, Mensaje) " +
                               "VALUES( @IdVenta, @PtoVta, @CbteTipo, @CbteTipo_desc, @Concepto,@Concepto_desc,
                                @DocTipo, @DocTipo_desc, @DocNro, @CbteDesde, @CbteHasta ,@CbteFch ,@ImpTotal ,@ImpTotConc ,@ImpNeto ,@ImpOpEx ,@ImpTrib ,@ImpIVA ,@MonId ,@MonCotiz ,@AlicIva_Id ,@AlicIva_BaseImp ,@AlicIva_Importe ,@Estado ,@TipoPago, @CAE, @CAE_vto, @Mensaje)"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()

        Dim id As Int64
        Comando.CommandText = "select @@identity"
        id = Comando.ExecuteScalar()

        Conexion.Close()
        Return id
    End Function

    Public Sub InsertarDetalleFactura()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Comando.Parameters.AddWithValue("@IdProducto", IdProducto)
        Comando.Parameters.AddWithValue("@Descripcion", Descripcion)
        Comando.Parameters.AddWithValue("@Marca", Marca)
        Comando.Parameters.AddWithValue("@Color", Color)
        Comando.Parameters.AddWithValue("@Talle", Talle)
        Comando.Parameters.AddWithValue("@Cantidad", Cantidad)
        Comando.Parameters.AddWithValue("@PrecioUnitario", PrecioUnitario)
        Comando.Parameters.AddWithValue("@Subtotal", Subtotal)
        Comando.Parameters.AddWithValue("@IdFactura", IdFactura)

        Comando.CommandText = "INSERT INTO DetallesFacturas(IdProducto, Descripcion, Marca, Color, Talle, Cantidad, PrecioUnitario, Subtotal, IdFactura) " +
                               "VALUES(@IdProducto ,@Descripcion ,@Marca ,@Color ,@Talle ,@Cantidad ,@PrecioUnitario ,@Subtotal ,@IdFactura)"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()
    End Sub

    Public Sub ActualizarFactura()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdFactura", IdFactura)
        Comando.Parameters.AddWithValue("@DocNro", DocNro)
        Comando.Parameters.AddWithValue("@CbteFch", CbteFch)
        Comando.Parameters.AddWithValue("@CbteDesde", CbteDesde)
        Comando.Parameters.AddWithValue("@CbteHasta", CbteHasta)
        Comando.Parameters.AddWithValue("@Estado", Estado)
        Comando.Parameters.AddWithValue("@CAE", CAE)
        Comando.Parameters.AddWithValue("@CAE_vto", CAE_vto)
        Comando.Parameters.AddWithValue("@Mensaje", Mensaje)

        Comando.CommandText = "UPDATE Facturas SET CbteFch=@CbteFch, DocNro=@DocNro, CbteDesde=@CbteDesde, CbteHasta=@CbteHasta, Estado=@Estado, CAE = @CAE," +
        "CAE_vto = @CAE_vto, Mensaje=@Mensaje WHERE IdFactura = @IdFactura"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerTodas() As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.CommandText = "select f.*,v.Descuento from Facturas f left join Ventas2 v on f.IdVenta = v.IdVenta where Convert(date, CbteFch) = Convert(date, GETDATE())"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function ObtenerTodasCliente() As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@DocNro", DocNro)

        comando.CommandText = "select f.*, v.Descuento from Facturas f left join Ventas2 v on f.IdVenta = v.IdVenta WHERE f.DocNro = @DocNro ORDER BY f.CbteFch DESC"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function ObtenerPorFecha(fd As Date, fh As Date) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@FechaDesde", fd)
        comando.Parameters.AddWithValue("@FechaHasta", fh)

        comando.CommandText = "select f.*, v.Descuento from Facturas f left join Ventas2 v on f.IdVenta = v.IdVenta " +
        "WHERE Convert(date, f.CbteFch) >= @FechaDesde AND Convert(date, f.CbteFch) <= @FechaHasta ORDER BY f.CbteFch DESC"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function ObtenerTodasMes() As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.CommandText = "select * from Facturas WHERE " +
                                "DATEPART(month,CbteFch) = MONTH(GETDATE()) AND DATEPART(YEAR,CbteFch) = YEAR(GETDATE())"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function ObtenerDetalles() As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@IdFactura", IdFactura)

        comando.CommandText = "select df.*,f.ImpTotal as Total from DetallesFacturas df " +
                                "inner join Facturas f on f.IdFactura=df.IdFactura " +
                                "WHERE df.IdFactura = @IdFactura"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

End Class
