﻿Public Class ModificarProducto

    Dim idprod As Int64
    Dim accion As String
    Dim codigo As String
    Dim dt As New DataTable

    Sub New(_idprod As Int64, _codigo As String, _accion As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        idprod = _idprod
        codigo = _codigo
        accion = _accion

        If accion = "M" Then
            lblTitulo.Text = "Modificar producto"
            Llenar_CombosBox()
            Cargar_Datos()
        ElseIf accion = "A" Then
            lblTitulo.Text = "Agregar producto"
            Llenar_CombosBox()
        End If

    End Sub

    Private Sub Llenar_CombosBox()
        Dim marca As New DatosProductos
        CBoxMarca.DataSource = marca.ObtenerTodoMarca()
        CBoxMarca.DisplayMember = "Nombre"
        CBoxMarca.ValueMember = "IdMarca"

        Dim talles As New DatosTalles
        CBoxTalle.DataSource = talles.ObtenerTodo
        CBoxTalle.DisplayMember = "Nombre"
        CBoxTalle.ValueMember = "IdTalle"
    End Sub

    Public Sub Cargar_Datos()

        Dim p As New DatosProductos

        If (idprod = 0) Then
            p.Codigo_ = codigo
            dt = p.BuscarxCodigo()
        Else
            p.IdProducto_ = idprod
            p.Buscar(dt, 6)
        End If

        If dt.Rows.Count <> 0 Then

            txtCod.Text = dt.Rows(0)("Codigo")
            CBoxMarca.SelectedIndex = CBoxMarca.FindStringExact(dt.Rows(0)("Marca"))
            txtDesc.Text = dt.Rows(0)("Descripcion")
            CBoxTalle.SelectedIndex = CBoxTalle.FindStringExact(dt.Rows(0)("Talle"))
            txtColor.Text = dt.Rows(0)("NombreColor")
            bttColor.BackColor = Color.FromArgb(Convert.ToInt32(dt.Rows(0)("Color")))
            txtPrecioMi.Text = dt.Rows(0)("PrecioMinorista")
            txtPrecioMa.Text = dt.Rows(0)("PrecioMayorista")
            txtPrecioTa.Text = dt.Rows(0)("PrecioTarjeta")
            NumCant.Value = dt.Rows(0)("Cantidad")

        End If

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        Dim ind As New DatosProductos

        If accion = "A" Then 'AGREGAR
#Region "Agregar"
            If CBoxMarca.SelectedIndex <> -1 And txtDesc.Text <> "" And CBoxTalle.SelectedIndex <> -1 And txtColor.Text <> "" And txtPrecioMa.Text <> "" And txtPrecioMi.Text <> "" And txtPrecioTa.Text <> "" And NumCant.Value <> 0 Then
                Try

                    ind.Marca_ = CBoxMarca.Text
                    ind.Descripcion_ = txtDesc.Text
                    ind.Talle_ = CBoxTalle.Text
                    ind.Color_ = bttColor.BackColor.ToArgb
                    ind.colorname_ = txtColor.Text
                    ind.PrecioMinorista_ = txtPrecioMi.Text
                    ind.PrecioMayorista_ = txtPrecioMa.Text
                    ind.PrecioTarjeta_ = txtPrecioTa.Text
                    ind.Cantidad_ = NumCant.Value

                    If (txtCod.Text <> "") Then

                        'se ingreso un codigo
                        ind.Codigo_ = Trim(txtCod.Text)
                        Dim buscar_producto = ind.BuscarxCodigo()

                        If buscar_producto.rows.count() > 0 Then 'verifica que no exista el codigo en la bd

                            'el codigo ingresado existe y se modificara la cantidad nada mas
                            ind.ModificarIndumentaria(idprod)
                        Else

                            'el codigo ingresado no existe y se hace una insercion normal
                            ind.InsertarIndumentaria()
                            MsgBox("Se agregó con éxito el producto.", MsgBoxStyle.Information, Title:="")
                        End If
                    Else
                        'no se ingreso ningun codigo y se genera uno
                        ind.Codigo_ = "i_" + (CInt(Math.Ceiling(Rnd() * 999999)) + 1).ToString + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss")
                        ind.InsertarIndumentaria()
                        MsgBox("Se agregó con éxito el producto.", MsgBoxStyle.Information, Title:="")
                    End If

                    Me.DialogResult = DialogResult.OK

                Catch ex As Exception
                    MessageBox.Show("Se produjo un error al guardar el producto. Detalle: " & ex.Message.ToString)
                End Try
            Else
                MessageBox.Show("Complete todos los campos.")
            End If
#End Region

        ElseIf accion = "M" Then 'MODIFICAR
#Region "Modificar"

            If CBoxMarca.SelectedIndex <> -1 And txtDesc.Text <> "" And CBoxTalle.SelectedIndex <> -1 And txtColor.Text <> "" And txtPrecioMa.Text <> "" And txtPrecioMi.Text <> "" And txtPrecioTa.Text <> "" Then
                Try
                    ind.Marca_ = CBoxMarca.Text
                    ind.Descripcion_ = txtDesc.Text
                    ind.Talle_ = CBoxTalle.Text
                    ind.Color_ = bttColor.BackColor.ToArgb
                    ind.colorname_ = txtColor.Text
                    ind.PrecioMinorista_ = txtPrecioMi.Text
                    ind.PrecioMayorista_ = txtPrecioMa.Text
                    ind.PrecioTarjeta_ = txtPrecioTa.Text
                    ind.Cantidad_ = NumCant.Value
                    If (txtCod.Text = "") Then
                        'Modificar Producto con Codigo Aleatorio
                        ind.Codigo_ = "i_" + (CInt(Math.Ceiling(Rnd() * 999999)) + 1).ToString + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss")
                        ind.ModificarIndumentaria(idprod)
                        MsgBox("Se modificó el producto con éxito.", MsgBoxStyle.Information, Title:="")
                    Else
                        ' que si el codigo es el mismoel dgv entonces no se modifico el txtCodigo
                        If txtCod.Text = dt.Rows(0)("Codigo") Then
                            'Modificar Producto con el mismo codigo
                            ind.Codigo_ = Trim(txtCod.Text)
                            ind.ModificarIndumentaria(idprod)
                            MsgBox("Se modificó el producto con éxito.", MsgBoxStyle.Information, Title:="")
                        Else
                            ind.Codigo_ = txtCod.Text
                            Dim buscar_producto = ind.BuscarxCodigo()
                            If buscar_producto.rows.count() > 0 Then
                                'Error por ingresar un Codigo Existente
                                MsgBox("El código del producto ya esta en uso. Deje el campo en blanco para generar un código interno.", MsgBoxStyle.Information, Title:="")
                                Return
                            Else
                                'Modificar Productos con un nuevo Codigo no Existente
                                ind.Codigo_ = Trim(txtCod.Text)
                                ind.ModificarIndumentaria(idprod)
                                MsgBox("Se modificó el producto con éxito.", MsgBoxStyle.Information, Title:="")
                            End If
                        End If

                        Me.DialogResult = DialogResult.OK

                    End If

                Catch ex As Exception
                    MessageBox.Show("Se produjo un error al modificar el producto. Detalle: " & ex.Message.ToString)
                End Try
            Else
                MessageBox.Show("Complete todos los campos.")
            End If


#End Region
        End If



    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim am As New AgregarMarca

        Try
            am.ShowDialog()
            Llenar_CombosBox()
            CBoxMarca.SelectedIndex = CBoxMarca.FindStringExact(am.txtMarca.Text)

        Catch ex As Exception
        End Try
    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Dispose()
    End Sub

    Private Sub bttColor_Click(sender As Object, e As EventArgs) Handles bttColor.Click
        ' Al hacer click para seleccionar el color, buscar paleta guardada y cargar colores
        ' Los colores se guardan en un archivo XML

        ' Si no existe el archivo se para la app al intentar leer, controlando ese caso
        If My.Computer.FileSystem.FileExists("colores.xml") Then

            ' colores: arreglo de Integer para ColorDialog
            ' coloresXml: objeto de donde se sacan los colores
            Dim colores(0) As Integer
            Dim coloresXml As XElement = XElement.Load("colores.xml")

            ' Por cada nodo (valor) en coloresXml...
            For Each color As XNode In coloresXml.Nodes
                Dim colorInt As Integer
                Dim colorStr = DirectCast(color, System.[Xml].Linq.XElement).Value

                Integer.TryParse(colorStr, colorInt)

                ' Agregar color al final de arreglo colores
                ' Si es el primer elemento (colores(0) siempre se crea con un elemento en 0)
                '   no redimencionar el arreglo.
                ' De lo contrario, redimencionar el arreglo para el nuevo elemento
                If (colores.Length = 1) Then
                    If (colores(0) <> 0) Then
                        Array.Resize(colores, colores.Length + 1)
                    End If
                Else
                    Array.Resize(colores, colores.Length + 1)
                End If

                colores(colores.Length - 1) = colorInt
            Next

            ' Setear colores en el dialog
            ColorDialog1.CustomColors = colores
        End If


        If ColorDialog1.ShowDialog() = DialogResult.OK Then

            ' Al apretar aceptar guardar los colores en colores.xml
            ' Se crea un nodo "root" en el archivo, y se guardan los valores
            ' en nodos hijos.
            Dim coloresAGuardar As New XElement("root")

            For Each color As Integer In ColorDialog1.CustomColors
                Dim nodo As New XElement("valor")
                nodo.Value = color

                coloresAGuardar.Add(nodo)
            Next

            coloresAGuardar.Save("colores.xml")

            bttColor.BackColor = ColorDialog1.Color
        End If
    End Sub

End Class