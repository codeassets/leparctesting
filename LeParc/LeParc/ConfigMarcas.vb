﻿Public Class ConfigMarcas

    Dim opcion As Integer

    Private Sub ConfigMarcas_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim marca As New DatosProductos
        dgvMarcas.DataSource = marca.ObtenerTodoMarca
        dgvMarcas.Columns("IdMarca").Visible = False
        dgvMarcas.Columns("Activo").Visible = False
        dgvMarcas.Columns("Nombre").HeaderText = "Marca"

    End Sub

    Private Sub dgvMarcas_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvMarcas.CellClick

        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtMarca.Text = dgvMarcas.CurrentRow.Cells("Nombre").Value
        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        ' Agregar
        txtMarca.Enabled = True
        txtMarca.Clear()
        txtMarca.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvMarcas.Enabled = False

        opcion = 1

    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click

        ' Modificar
        txtMarca.Enabled = True
        txtMarca.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvMarcas.Enabled = False

        opcion = 2

    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este registro?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                Dim marca As New DatosProductos
                marca.IdMarca_ = dgvMarcas.SelectedRows(0).Cells("IdMarca").Value
                marca.EliminarMarca()
                MsgBox("Se eliminó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvMarcas.Enabled = True
                dgvMarcas.DataSource = marca.ObtenerTodoMarca()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el registro. Detalle:" & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        dgvMarcas.Enabled = True
        dgvMarcas.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtMarca.Enabled = False
        txtMarca.Clear()

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtMarca.Text <> "" Then

            Dim marca As New DatosProductos

            If opcion = 1 Then 'AGREGAR
                Try
                    marca.Marca_ = txtMarca.Text
                    marca.InsertarMarca()
                    MsgBox("Registro creado con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvMarcas.Enabled = True
                    dgvMarcas.DataSource = marca.ObtenerTodoMarca()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el registro. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try

                    marca.Marca_ = txtMarca.Text
                    marca.IdMarca_ = dgvMarcas.SelectedRows(0).Cells("IdMarca").Value
                    marca.ModificarMarca()
                    MsgBox("Se modificó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvMarcas.Enabled = True
                    dgvMarcas.DataSource = marca.ObtenerTodoMarca

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el registro. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If

    End Sub
End Class