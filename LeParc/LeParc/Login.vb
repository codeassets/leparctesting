﻿Imports System.Data.SqlClient

Public Class Login

        Private strConn As String = "Data Source=190.105.225.136\MSSQLSERVER2012;Initial Catalog=CodeAssets;Persist Security Info=True;User ID=codeassets;Password=%p95O6qg"
        Private sqlCon As SqlConnection

        Private Class ModeloLogin

            Private email_ As String
            Public Property email() As String
                Get
                    Return email_
                End Get
                Set(ByVal value As String)
                    email_ = value
                End Set
            End Property

            Private password_ As String
            Public Property password() As String
                Get
                    Return password_
                End Get
                Set(ByVal value As String)
                    password_ = value
                End Set
            End Property

        End Class

        Private Class ModeloRespuestaApi

            Private token_ As String
            Public Property token() As String
                Get
                    Return token_
                End Get
                Set(ByVal value As String)
                    token_ = value
                End Set
            End Property

            Private expiracion_ As String
            Public Property expiracion() As String
                Get
                    Return expiracion_
                End Get
                Set(ByVal value As String)
                    expiracion_ = value
                End Set
            End Property

        End Class

        Private Sub bttSalir_Click(sender As Object, e As EventArgs) Handles bttSalir.Click
            Me.Close()
        End Sub

        Private Async Sub bttAceptar_Click(sender As Object, e As EventArgs) Handles bttAceptar.Click

            sqlCon = New SqlConnection(strConn)

            Dim resultado As New List(Of String)

            Using (sqlCon)

                Dim sqlComm As New SqlCommand()

                sqlComm.Connection = sqlCon

                sqlComm.CommandText = "VerificarUsuario"
                sqlComm.CommandType = CommandType.StoredProcedure

                sqlComm.Parameters.AddWithValue("usuario", txtUsuario.Text)
                sqlComm.Parameters.AddWithValue("password", txtPassword.Text)
                sqlComm.Parameters.AddWithValue("device_id", "asd123")
            sqlComm.Parameters.AddWithValue("id_sistema", "2")

            sqlCon.Open()

                sqlComm.ExecuteNonQuery()

                Dim reader As SqlDataReader

                reader = sqlComm.ExecuteReader()
                While (reader.Read())
                    resultado.Add(reader("estado").ToString)
                    resultado.Add(reader("descripcion").ToString)
                End While

                sqlCon.Close()

            End Using

            '' Login correcto
            If (resultado(0) = "correcto") Then

                sqlCon = New SqlConnection(strConn)

                '' Controlar cuota
                Using (sqlCon)

                    Dim dt As New DataTable
                    Dim da As New SqlDataAdapter("VerCuotasImpagasPorSistema", strConn)
                    da.SelectCommand.CommandType = CommandType.StoredProcedure
                da.SelectCommand.Parameters.AddWithValue("id_sistema", "2")
                da.Fill(dt)

                If (dt.Rows.Count >
                    1) Then
                    MessageBox.Show("Por favor abone la cuota del servicio para continuar utilizando el sistema.")
                Else
                    Me.DialogResult = DialogResult.OK
                    End If

                End Using


            Else
                '' Login incorrecto
                MessageBox.Show("Usuario o contraseña incorrectos")

            End If

        End Sub

        Private Sub Login_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        txtPassword.Focus()
    End Sub

        Private Sub txtPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPassword.KeyDown
            If (e.KeyCode = Keys.Enter) Then
                bttAceptar.PerformClick()
            End If
        End Sub


        Private Sub Login_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
            If (e.KeyCode = Keys.Escape) Then
                bttSalir.PerformClick()
            End If
        End Sub


    End Class
