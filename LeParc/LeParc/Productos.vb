﻿Imports System.ComponentModel

Public Class Productos

    Dim opcion As Integer
    Dim color As Color
    Dim idprod As Integer

    Dim idColumnaSeleccionada As Integer

    Public Sub CancelAction()
        bttAgregarProd.Enabled = True
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False
        bttCancelar.Enabled = False
        TLP_Datos.Enabled = False

        dgvStock.Enabled = True
        dgvStock.ClearSelection()
        Limpiar_Campos()

        For Each row As DataGridViewRow In dgvStock.Rows
            If row.Cells("IdProducto").Value = idColumnaSeleccionada Then
                dgvStock.Rows(row.Index).Selected = True
                txtCod.Text = dgvStock.SelectedRows(0).Cells("Codigo").Value
                CBoxMarca.Text = dgvStock.SelectedRows(0).Cells("Marca").Value
                txtDesc.Text = dgvStock.SelectedRows(0).Cells("Descripcion").Value
                CBoxTalle.Text = dgvStock.SelectedRows(0).Cells("Talle").Value
                bttColor.BackColor = color.FromArgb(Convert.ToInt32(dgvStock.SelectedRows(0).Cells("Color").Value))
                tb_colorname.Text = dgvStock.SelectedRows(0).Cells("NombreColor").Value
                txtPrecioMi.Text = dgvStock.SelectedRows(0).Cells("PrecioMinorista").Value
                txtPrecioMa.Text = dgvStock.SelectedRows(0).Cells("PrecioMayorista").Value
                txtPrecioTa.Text = dgvStock.SelectedRows(0).Cells("PrecioTarjeta").Value
                NumCant.Value = dgvStock.SelectedRows(0).Cells("Cantidad").Value
                Exit For
            End If
        Next
        scrollGrid()
    End Sub

    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer

        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt

    End Function

    Public Sub scrollGrid()

        Try
            Dim halfway As Integer = dgvStock.DisplayedRowCount(False) / 2
            If dgvStock.FirstDisplayedScrollingRowIndex + halfway > dgvStock.SelectedRows(0).Index Or dgvStock.FirstDisplayedScrollingRowIndex + dgvStock.DisplayedRowCount(False) - halfway <= dgvStock.SelectedRows(0).Index Then
                Dim targetRow As Integer = dgvStock.SelectedRows(0).Index
                targetRow = Math.Max(targetRow - halfway, 0)
                dgvStock.FirstDisplayedScrollingRowIndex = targetRow

            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub LLenar_Producto_EN_ORDEN()

        Dim indu As New DatosProductos

        Dim dt As New DataTable
        Dim Bandera As New Integer

        If txtBuscar.Text <> "" Then
            If CB_Buscar.SelectedIndex = 0 Then 'codigo
                indu.Codigo_ = Trim(txtBuscar.Text)
                Bandera = 0

            ElseIf CB_Buscar.SelectedIndex = 1 Then 'talle
                indu.Talle_ = txtBuscar.Text
                Bandera = 1

            ElseIf CB_Buscar.SelectedIndex = 2 Then 'marca
                indu.Marca_ = txtBuscar.Text
                Bandera = 2
            ElseIf CB_Buscar.SelectedIndex = 3 Then 'color
                indu.colorname_ = txtBuscar.Text
                Bandera = 3
            ElseIf CB_Buscar.SelectedIndex = 4 Then 'descripcion
                indu.Descripcion_ = txtBuscar.Text
                Bandera = 4
            ElseIf CB_Buscar.SelectedIndex = 5 Then 'todos
                If (txtBuscar.Text.Contains(" ")) Then
                    indu.Descripcion_ = txtBuscar.Text.Split(" ")(0).Replace(" ", "")
                    indu.Marca_ = txtBuscar.Text.Split(" ")(1).Replace(" ", "")
                Else
                    indu.Descripcion_ = txtBuscar.Text
                End If

                Bandera = 5
            End If
            dgvStock.DataSource = indu.TraerOrdenado(dt, Bandera)
        Else
            dgvStock.DataSource = indu.ObtenerTodo()
        End If

        If (columnxy <> Nothing) Then

            dgvStock.Sort(dgvStock.Columns(columnxy), SetSortOrder)

        End If

        dgvStock.Columns("IdProducto").Visible = False
        dgvStock.Columns("Color").Visible = False
        dgvStock.Columns("Activo").Visible = False
        dgvStock.Columns("IdUsuario_A").Visible = False
        dgvStock.Columns("IdUsuario_M").Visible = False
        dgvStock.Columns("Fecha_A").Visible = False
        dgvStock.Columns("Fecha_M").Visible = False

        If dgvStock.RowCount <> 0 Then

            For Each row As DataGridViewRow In dgvStock.Rows
                row.Cells("NombreColor").Style.BackColor = color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
            Next

            For Each row As DataGridViewRow In dgvStock.Rows
                If row.Cells("IdProducto").Value = idColumnaSeleccionada Then
                    'dgvStock.CurrentCell = dgvStock.Rows(row.Index).Cells(1)
                    'dgvStock.Rows(row.Index).Selected = True
                    dgvStock.Rows(row.Index).Cells("Descripcion").Selected = True
                    txtCod.Text = dgvStock.SelectedRows(0).Cells("Codigo").Value
                    CBoxMarca.Text = dgvStock.SelectedRows(0).Cells("Marca").Value
                    txtDesc.Text = dgvStock.SelectedRows(0).Cells("Descripcion").Value
                    CBoxTalle.Text = dgvStock.SelectedRows(0).Cells("Talle").Value
                    bttColor.BackColor = color.FromArgb(Convert.ToInt32(dgvStock.SelectedRows(0).Cells("Color").Value))
                    tb_colorname.Text = dgvStock.SelectedRows(0).Cells("NombreColor").Value
                    txtPrecioMi.Text = dgvStock.SelectedRows(0).Cells("PrecioMinorista").Value
                    txtPrecioMa.Text = dgvStock.SelectedRows(0).Cells("PrecioMayorista").Value
                    txtPrecioTa.Text = dgvStock.SelectedRows(0).Cells("PrecioTarjeta").Value
                    NumCant.Value = dgvStock.SelectedRows(0).Cells("Cantidad").Value
                    Exit For
                End If
            Next

        End If

        dgvStock.Columns("Codigo").FillWeight = 10
        dgvStock.Columns("Descripcion").FillWeight = 20
        dgvStock.Columns("Marca").FillWeight = 10
        dgvStock.Columns("Cantidad").FillWeight = 5
        dgvStock.Columns("Talle").FillWeight = 8
        dgvStock.Columns("NombreColor").FillWeight = 10
        dgvStock.Columns("PrecioMinorista").FillWeight = 12
        dgvStock.Columns("PrecioMayorista").FillWeight = 12
        dgvStock.Columns("PrecioTarjeta").FillWeight = 12

        dgvStock.Columns("NombreColor").HeaderText = "Color"
        dgvStock.Columns("PrecioMinorista").HeaderText = "$ Min."
        dgvStock.Columns("PrecioMayorista").HeaderText = "$ May."
        dgvStock.Columns("PrecioTarjeta").HeaderText = "$ Tarj."
        dgvStock.Columns("Cantidad").HeaderText = "Cant."
        scrollGrid()

    End Sub



    Private Sub Llenar_CombosBox()
        Dim marca As New DatosProductos
        CBoxMarca.DataSource = marca.ObtenerTodoMarca()
        CBoxMarca.DisplayMember = "Nombre"
        CBoxMarca.ValueMember = "IdMarca"

        CBoxMarca.SelectedIndex = -1

        Dim talles As New DatosTalles
        CBoxTalle.DataSource = talles.ObtenerTodo
        CBoxTalle.DisplayMember = "Nombre"
        CBoxTalle.ValueMember = "IdTalle"
        CBoxMarca.SelectedIndex = -1
    End Sub

    Private Sub Limpiar_Campos()
        Try
            For Each control As Control In TLP_Datos.Controls
                NumCant.Value = 0
                CBoxTalle.SelectedIndex = -1
                bttColor.BackColor = color
                tb_colorname.Text = ""
                If TypeOf control Is TextBox Then
                    CType(control, TextBox).Clear()
                ElseIf TypeOf control Is CheckBox Then
                    CType(control, CheckBox).Checked = False
                ElseIf TypeOf control Is ComboBox Then
                    CType(control, ComboBox).SelectedIndex = -1
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub bttColor_Click(sender As Object, e As EventArgs) Handles bttColor.Click
        ' Al hacer click para seleccionar el color, buscar paleta guardada y cargar colores
        ' Los colores se guardan en un archivo XML

        ' Si no existe el archivo se para la app al intentar leer, controlando ese caso
        If My.Computer.FileSystem.FileExists("colores.xml") Then

            ' colores: arreglo de Integer para ColorDialog
            ' coloresXml: objeto de donde se sacan los colores
            Dim colores(0) As Integer
            Dim coloresXml As XElement = XElement.Load("colores.xml")

            ' Por cada nodo (valor) en coloresXml...
            For Each color As XNode In coloresXml.Nodes
                Dim colorInt As Integer
                Dim colorStr = DirectCast(color, System.[Xml].Linq.XElement).Value

                Integer.TryParse(colorStr, colorInt)

                ' Agregar color al final de arreglo colores
                ' Si es el primer elemento (colores(0) siempre se crea con un elemento en 0)
                '   no redimencionar el arreglo.
                ' De lo contrario, redimencionar el arreglo para el nuevo elemento
                If (colores.Length = 1) Then
                    If (colores(0) <> 0) Then
                        Array.Resize(colores, colores.Length + 1)
                    End If
                Else
                    Array.Resize(colores, colores.Length + 1)
                End If

                colores(colores.Length - 1) = colorInt
            Next

            ' Setear colores en el dialog
            ColorDialog1.CustomColors = colores
        End If


        If ColorDialog1.ShowDialog() = DialogResult.OK Then

            ' Al apretar aceptar guardar los colores en colores.xml
            ' Se crea un nodo "root" en el archivo, y se guardan los valores
            ' en nodos hijos.
            Dim coloresAGuardar As New XElement("root")

            For Each color As Integer In ColorDialog1.CustomColors
                Dim nodo As New XElement("valor")
                nodo.Value = color

                coloresAGuardar.Add(nodo)
            Next

            coloresAGuardar.Save("colores.xml")

            bttColor.BackColor = ColorDialog1.Color
        End If
    End Sub

    Private Sub bttAgregarProd_Click(sender As Object, e As EventArgs) Handles bttAgregarProd.Click
        Limpiar_Campos()
        TLP_Datos.Enabled = True
        txtCod.Focus()
        bttAgregarProd.Enabled = False
        bttEditarProd.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvStock.Enabled = False
        If dgvStock.SelectedRows.Count <> 0 Then
            idColumnaSeleccionada = dgvStock.SelectedRows(0).Cells("IdProducto").Value
        End If
        opcion = 1
    End Sub

    Private Sub bttEditarProd_Click(sender As Object, e As EventArgs) Handles bttEditarProd.Click
        Try
            opcion = 2
            TLP_Datos.Enabled = True
            bttAgregarProd.Enabled = False
            bttEditarProd.Enabled = False
            bttEliminar.Enabled = False

            bttGuardar.Enabled = True
            bttCancelar.Enabled = True
            dgvStock.Enabled = False
            idColumnaSeleccionada = dgvStock.SelectedRows(0).Cells("IdProducto").Value
        Catch ex As Exception
            MessageBox.Show("Seleccione el producto que desea modificar. Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                If Asc(e.KeyChar) = 44 Then
                    Dim count As Integer = CountCharacter(txtPrecioMi.Text, e.KeyChar)
                    If count <> 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If

            End If
        End If
    End Sub

    Private Sub Productos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        color = bttColor.BackColor

        Llenar_CombosBox()

        CB_Buscar.SelectedIndex = 0

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim am As New AgregarMarca

        Try
            am.ShowDialog()

        Catch ex As Exception
        End Try
        Llenar_CombosBox()
        CBoxMarca.SelectedIndex = CBoxMarca.FindStringExact(am.txtMarca.Text)
    End Sub

    Private Sub Productos_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If bttCancelar.Enabled = True Then
            If e.KeyCode = Keys.Escape Then CancelAction()
        End If
    End Sub


    Private Sub txtCod_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles txtCod.PreviewKeyDown
        If e.KeyCode = Keys.Enter Then
            Dim obj_validar As New DatosProductos

            If TLP_Datos.Enabled = True Then
                obj_validar.Codigo_ = txtCod.Text
                Dim Resultado_Existencia As Boolean = obj_validar.VerificarCodBarras()
                If Resultado_Existencia = True Then
                    Dim prod As New DatosProductos
                    Dim dt As DataTable
                    prod.Codigo_ = txtCod.Text
                    dt = prod.BuscarxCodigo()
                    CBoxMarca.Text = dt.Rows(0)("Marca")
                    txtDesc.Text = dt.Rows(0)("Descripcion")
                    CBoxTalle.Text = dt.Rows(0)("Talle")
                    bttColor.BackColor = Color.FromArgb(Convert.ToInt32(dt.Rows(0)("Color")))
                    tb_colorname.Text = dt.Rows(0)("NombreColor")
                    txtPrecioMi.Text = dt.Rows(0)("PrecioMinorista")
                    txtPrecioMa.Text = dt.Rows(0)("PrecioMayorista")
                    txtPrecioTa.Text = dt.Rows(0)("PrecioTarjeta")
                    NumCant.Value = dt.Rows(0)("Cantidad")
                    idprod = dt.Rows(0)("IdProducto")

                    'desactiva los campos para que no pueda modificar mas que la cantidad
                    CBoxMarca.Enabled = False
                    txtDesc.Enabled = False
                    CBoxTalle.Enabled = False
                    bttColor.Enabled = False
                    tb_colorname.Enabled = False
                    txtPrecioMi.Enabled = False
                    txtPrecioMa.Enabled = False
                    txtPrecioTa.Enabled = False
                    NumCant.Enabled = True
                    NumCant.Focus()

                Else
                    CBoxMarca.Enabled = True
                    txtDesc.Enabled = True
                    CBoxTalle.Enabled = True
                    bttColor.Enabled = True
                    tb_colorname.Enabled = True
                    txtPrecioMi.Enabled = True
                    txtPrecioMa.Enabled = True
                    txtPrecioTa.Enabled = True
                    NumCant.Enabled = True

                End If
            End If
        End If

    End Sub

    Private Sub Productos_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        If FormPrincipal.Width > 1300 Then
            bttAgregarProd.Text = "Agregar"
            bttEditarProd.Text = "Modificar"
            bttEliminar.Text = "Eliminar"
            bttAgregarProd.Size = New Size(121, 44)
            bttEditarProd.Size = New Size(121, 44)
            bttEliminar.Size = New Size(121, 44)
        Else
            bttAgregarProd.Text = ""
            bttEditarProd.Text = ""
            bttEliminar.Text = ""
        End If
    End Sub



    Private Sub txtPrecioMi_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioMi.KeyPress

        If [Char].IsDigit(e.KeyChar) OrElse [Char].IsControl(e.KeyChar) Then

        ElseIf [Char].IsPunctuation(e.KeyChar) Then
            If (txtPrecioMi.Text.Contains(",")) Then
                e.Handled = True
            End If
        Else
            e.Handled = True
        End If

    End Sub

    Private Sub txtPrecioMa_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioMa.KeyPress

        If [Char].IsDigit(e.KeyChar) OrElse [Char].IsControl(e.KeyChar) Then

        ElseIf [Char].IsPunctuation(e.KeyChar) Then
            If (txtPrecioMa.Text.Contains(",")) Then
                e.Handled = True
            End If
        Else
            e.Handled = True
        End If

    End Sub

    Private Sub txtPrecioTa_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioTa.KeyPress

        If [Char].IsDigit(e.KeyChar) OrElse [Char].IsControl(e.KeyChar) Then

        ElseIf [Char].IsPunctuation(e.KeyChar) Then
            If (txtPrecioTa.Text.Contains(",")) Then
                e.Handled = True
            End If
        Else
            e.Handled = True
        End If

    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim ind As New DatosProductos
        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este producto del inventario?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                ind.IdProducto_ = dgvStock.SelectedRows(0).Cells("IdProducto").Value
                ind.EliminarProducto()
                MsgBox("Se eliminó el producto con éxito.", MsgBoxStyle.Information, Title:="")
                dgvStock.Enabled = True
                LLenar_Producto_EN_ORDEN()
                CancelAction()
                txtBuscar.Clear()
            Catch ex As Exception
                MessageBox.Show("Se produjo un error al eliminar el producto. Detalle: " & ex.Message.ToString)
            End Try
        End If

    End Sub

    Dim columnxy As String
    Dim SetSortOrder As ListSortDirection
    Dim GridSortOrder As SortOrder


    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim indu As New DatosProductos

            'buscar por el combobox

            Try

                If (txtBuscar.Text <> "") Then
                    Dim dt As New DataTable
                    Dim Bandera As New Integer

                    If CB_Buscar.SelectedIndex = 0 Then 'codigo
                        indu.Codigo_ = txtBuscar.Text
                        Bandera = 0

                    ElseIf CB_Buscar.SelectedIndex = 1 Then 'talle
                        indu.Talle_ = txtBuscar.Text
                        Bandera = 1

                    ElseIf CB_Buscar.SelectedIndex = 2 Then 'marca
                        indu.Marca_ = txtBuscar.Text
                        Bandera = 2
                    ElseIf CB_Buscar.SelectedIndex = 3 Then 'color
                        indu.colorname_ = txtBuscar.Text
                        Bandera = 3
                    ElseIf CB_Buscar.SelectedIndex = 4 Then 'descripcion
                        indu.Descripcion_ = txtBuscar.Text
                        Bandera = 4
                    ElseIf CB_Buscar.SelectedIndex = 5 Then 'todos
                        If (txtBuscar.Text.Contains(" ")) Then
                            indu.Descripcion_ = txtBuscar.Text.Split(" ")(0).Replace(" ", "")
                            indu.Marca_ = txtBuscar.Text.Split(" ")(1).Replace(" ", "")
                        Else
                            indu.Descripcion_ = txtBuscar.Text
                        End If

                        Bandera = 5
                    End If


                    dgvStock.DataSource = indu.Buscar(dt, Bandera)
                Else
                    dgvStock.DataSource = indu.ObtenerTodo()
                End If

                dgvStock.Columns("IdProducto").Visible = False
                dgvStock.Columns("Color").Visible = False
                dgvStock.Columns("Activo").Visible = False
                dgvStock.Columns("IdUsuario_A").Visible = False
                dgvStock.Columns("IdUsuario_M").Visible = False
                dgvStock.Columns("Fecha_A").Visible = False
                dgvStock.Columns("Fecha_M").Visible = False

                If dgvStock.RowCount <> 0 Then
                    For Each row As DataGridViewRow In dgvStock.Rows
                        row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
                    Next
                End If

                dgvStock.Columns("Codigo").FillWeight = 5
                dgvStock.Columns("Descripcion").FillWeight = 21
                dgvStock.Columns("Marca").FillWeight = 10
                dgvStock.Columns("Cantidad").FillWeight = 10
                dgvStock.Columns("Talle").FillWeight = 8
                dgvStock.Columns("NombreColor").FillWeight = 10
                dgvStock.Columns("PrecioMinorista").FillWeight = 12
                dgvStock.Columns("PrecioMayorista").FillWeight = 12
                dgvStock.Columns("PrecioTarjeta").FillWeight = 12

                dgvStock.Columns("NombreColor").HeaderText = "Color"
                dgvStock.Columns("PrecioMinorista").HeaderText = "$ Min."
                dgvStock.Columns("PrecioMayorista").HeaderText = "$ May."
                dgvStock.Columns("PrecioTarjeta").HeaderText = "$ Tarj."
                dgvStock.Columns("Cantidad").HeaderText = "Cant."
                dgvStock.ClearSelection()
            Catch ex As Exception
                MessageBox.Show("Ocurrio un error al realizar la busqueda. Detalles: " & ex.Message.ToString)
            End Try
        End If
    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave
        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If
    End Sub

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click
        Dim ind As New DatosProductos

        If opcion = 1 Then 'AGREGAR
#Region "Agregar"
            If CBoxMarca.SelectedIndex <> -1 And txtDesc.Text <> "" And CBoxTalle.SelectedIndex <> -1 And tb_colorname.Text <> "" And txtPrecioMa.Text <> "" And txtPrecioMi.Text <> "" And txtPrecioTa.Text <> "" And NumCant.Value <> 0 Then
                Try

                    ind.Marca_ = CBoxMarca.Text
                    ind.Descripcion_ = txtDesc.Text
                    ind.Talle_ = CBoxTalle.Text
                    ind.Color_ = bttColor.BackColor.ToArgb
                    ind.colorname_ = tb_colorname.Text
                    ind.PrecioMinorista_ = txtPrecioMi.Text
                    ind.PrecioMayorista_ = txtPrecioMa.Text
                    ind.PrecioTarjeta_ = txtPrecioTa.Text
                    ind.Cantidad_ = NumCant.Value

                    If (txtCod.Text <> "") Then

                        'se ingreso un codigo
                        ind.Codigo_ = Trim(txtCod.Text)
                        Dim buscar_producto = ind.BuscarxCodigo()

                        If buscar_producto.rows.count() > 0 Then 'verifica que no exista el codigo en la bd

                            'el codigo ingresado existe y se modificara la cantidad nada mas
                            ind.ModificarIndumentaria(idprod)
                        Else

                            'el codigo ingresado no existe y se hace una insercion normal
                            ind.InsertarIndumentaria()
                            MsgBox("Se agregó con éxito el producto.", MsgBoxStyle.Information, Title:="")
                        End If
                    Else
                        'no se ingreso ningun codigo y se genera uno
                        ind.Codigo_ = "i_" + (CInt(Math.Ceiling(Rnd() * 999999)) + 1).ToString + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss")
                        ind.InsertarIndumentaria()
                        MsgBox("Se agregó con éxito el producto.", MsgBoxStyle.Information, Title:="")
                    End If

                    dgvStock.Enabled = True
                    'LLenar_Producto_EN_ORDEN()
                    'dgvStock.ClearSelection()
                    dgvStock.Enabled = False
                    Limpiar_Campos()
                    CBoxMarca.SelectedIndex = -1
                    txtCod.Focus()

                    'activar todos los campos
                    CBoxMarca.Enabled = True
                    txtDesc.Enabled = True
                    CBoxTalle.Enabled = True
                    bttColor.Enabled = True
                    tb_colorname.Enabled = True
                    txtPrecioMi.Enabled = True
                    txtPrecioMa.Enabled = True
                    txtPrecioTa.Enabled = True
                    NumCant.Enabled = True

                Catch ex As Exception
                    MessageBox.Show("Se produjo un error al guardar el producto. Detalle: " & ex.Message.ToString)
                End Try
            Else
                MessageBox.Show("Complete todos los campos.")
            End If
#End Region

        ElseIf opcion = 2 Then 'MODIFICAR
#Region "Modificar"

            If CBoxMarca.SelectedIndex <> -1 And txtDesc.Text <> "" And CBoxTalle.SelectedIndex <> -1 And tb_colorname.Text <> "" And txtPrecioMa.Text <> "" And txtPrecioMi.Text <> "" And txtPrecioTa.Text <> "" Then
                Try


                    ind.Marca_ = CBoxMarca.Text
                    ind.Descripcion_ = txtDesc.Text
                    ind.Talle_ = CBoxTalle.Text
                    ind.Color_ = bttColor.BackColor.ToArgb
                    ind.colorname_ = tb_colorname.Text
                    ind.PrecioMinorista_ = txtPrecioMi.Text
                    ind.PrecioMayorista_ = txtPrecioMa.Text
                    ind.PrecioTarjeta_ = txtPrecioTa.Text
                    ind.Cantidad_ = NumCant.Value
                    If (txtCod.Text = "") Then
                        'Modificar Producto con Codigo Aleatorio
                        ind.Codigo_ = "i_" + (CInt(Math.Ceiling(Rnd() * 999999)) + 1).ToString + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss")
                        ind.ModificarIndumentaria(dgvStock.SelectedRows(0).Cells("IdProducto").Value)
                        MsgBox("Se modificó el producto con éxito.", MsgBoxStyle.Information, Title:="")
                    Else
                        'si el codigo es el mismo que el dgv entonces no se modifico el txtCodigo
                        If txtCod.Text = dgvStock.SelectedRows(0).Cells("Codigo").Value Then
                            'Modificar Producto con el mismo codigo
                            ind.Codigo_ = Trim(txtCod.Text)
                            ind.ModificarIndumentaria(dgvStock.SelectedRows(0).Cells("IdProducto").Value)
                            MsgBox("Se modificó el producto con éxito.", MsgBoxStyle.Information, Title:="")
                        Else
                            ind.Codigo_ = txtCod.Text
                            Dim buscar_producto = ind.BuscarxCodigo()
                            If buscar_producto.rows.count() > 0 Then
                                'Error por ingresar un Codigo Existente
                                MsgBox("El código del producto ya esta en uso. Deje el campo en blanco para generar un código interno.", MsgBoxStyle.Information, Title:="")
                                Return
                            Else
                                'Modificar Productos con un nuevo Codigo no Existente
                                ind.Codigo_ = Trim(txtCod.Text)
                                ind.ModificarIndumentaria(dgvStock.SelectedRows(0).Cells("IdProducto").Value)
                                MsgBox("Se modificó el producto con éxito.", MsgBoxStyle.Information, Title:="")
                            End If
                        End If

                    End If
                    dgvStock.Enabled = True
                    LLenar_Producto_EN_ORDEN()
                    CancelAction()

                Catch ex As Exception
                    MessageBox.Show("Se produjo un error al modificar el producto. Detalle: " & ex.Message.ToString)
                End Try
            Else
                MessageBox.Show("Complete todos los campos.")
            End If


#End Region
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Dim ind As New DatosProductos
        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea cancelar esta operación?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then

            bttAgregarProd.Enabled = True
            bttEliminar.Enabled = False
            bttGuardar.Enabled = False
            bttCancelar.Enabled = False
            TLP_Datos.Enabled = False

            dgvStock.Enabled = True
            dgvStock.ClearSelection()
            Limpiar_Campos()

            For Each row As DataGridViewRow In dgvStock.Rows
                If row.Cells("IdProducto").Value = idColumnaSeleccionada Then
                    dgvStock.Rows(row.Index).Selected = True
                    Exit For
                End If
            Next
            scrollGrid()

        End If
    End Sub

    Private Sub bttFaltantes_Click(sender As Object, e As EventArgs) Handles bttFaltantes.Click
        Dim f As New Faltantes

        Try
            f.ShowDialog()
            '' ^ Agregue (Me) para evitar que la ventana desaparezca,
            '' pero tiraba error cuando se cerraba. No tengo idea de como solucionarlo 
        Catch ex As Exception

            MessageBox.Show(ex.Message.ToString)

        End Try
    End Sub

    Private Sub dgvStock_Sorted_1(sender As Object, e As EventArgs) Handles dgvStock.Sorted

        Try
            If dgvStock.RowCount <> 0 Then
                For Each row As DataGridViewRow In dgvStock.Rows
                    row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
                Next
            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema durante el ordenamiento. Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub dgvStock_ColumnHeaderMouseClick_1(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvStock.ColumnHeaderMouseClick

        columnxy = dgvStock.SortedColumn.Name
        GridSortOrder = dgvStock.SortOrder

        If GridSortOrder = Windows.Forms.SortOrder.Ascending Then
            SetSortOrder = ListSortDirection.Ascending
        ElseIf GridSortOrder = Windows.Forms.SortOrder.Descending Then
            SetSortOrder = ListSortDirection.Descending
        ElseIf GridSortOrder = Windows.Forms.SortOrder.None Then
            SetSortOrder = ListSortDirection.Ascending
        Else : GridSortOrder = ListSortDirection.Ascending
            MsgBox("Ocurrio un error")
        End If

    End Sub

    Private Sub dgvStock_CurrentCellChanged_1(sender As Object, e As EventArgs) Handles dgvStock.CurrentCellChanged

        Try
            If dgvStock.SelectedRows.Count <> 0 Then

                bttEditarProd.Enabled = True
                bttEliminar.Enabled = True
                txtCod.Text = dgvStock.SelectedRows(0).Cells("Codigo").Value
                CBoxMarca.Text = dgvStock.SelectedRows(0).Cells("Marca").Value
                txtDesc.Text = dgvStock.SelectedRows(0).Cells("Descripcion").Value
                CBoxTalle.Text = dgvStock.SelectedRows(0).Cells("Talle").Value
                bttColor.BackColor = Color.FromArgb(Convert.ToInt32(dgvStock.SelectedRows(0).Cells("Color").Value))
                tb_colorname.Text = dgvStock.SelectedRows(0).Cells("NombreColor").Value
                txtPrecioMi.Text = dgvStock.SelectedRows(0).Cells("PrecioMinorista").Value
                txtPrecioMa.Text = dgvStock.SelectedRows(0).Cells("PrecioMayorista").Value
                txtPrecioTa.Text = dgvStock.SelectedRows(0).Cells("PrecioTarjeta").Value
                NumCant.Value = dgvStock.SelectedRows(0).Cells("Cantidad").Value
            Else
                bttEditarProd.Enabled = False
                bttEliminar.Enabled = False
                Limpiar_Campos()
            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema al seleccionar la fila. Detalle: " & ex.Message.ToString)
        End Try

    End Sub

End Class