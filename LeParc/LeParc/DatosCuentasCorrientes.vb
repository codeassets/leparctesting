﻿Imports System.Data.SqlClient

Public Class DatosCuentasCorrientes

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

#Region "GetSet"

    Private IdCuenta As Int64
    Public Property IdCuenta_() As Int64
        Get
            Return IdCuenta
        End Get
        Set(ByVal value As Int64)
            IdCuenta = value
        End Set
    End Property

    Private IdCliente As Int64
    Public Property IdCliente_() As Int64
        Get
            Return IdCliente
        End Get
        Set(ByVal value As Int64)
            IdCliente = value
        End Set
    End Property

    Private Saldo As Double
    Public Property Saldo_() As Double
        Get
            Return Saldo
        End Get
        Set(ByVal value As Double)
            Saldo = value
        End Set
    End Property

    Private IdDetalle As Int64
    Public Property IdDetalle_() As Int64
        Get
            Return IdDetalle
        End Get
        Set(ByVal value As Int64)
            IdDetalle = value
        End Set
    End Property

    Private Monto As Double
    Public Property Monto_() As Double
        Get
            Return Monto
        End Get
        Set(ByVal value As Double)
            Monto = value
        End Set
    End Property

    Private Tipo As String
    Public Property Tipo_() As String
        Get
            Return Tipo
        End Get
        Set(ByVal value As String)
            Tipo = value
        End Set
    End Property

    Private Fecha As DateTime
    Public Property Fecha_() As DateTime
        Get
            Return Fecha
        End Get
        Set(ByVal value As DateTime)
            Fecha = value
        End Set
    End Property

    Private IdVenta As Int64
    Public Property IdVenta_() As Int64
        Get
            Return IdVenta
        End Get
        Set(ByVal value As Int64)
            IdVenta = value
        End Set
    End Property

#End Region

    Public Function VerificarCuenta() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Dim dt As New DataTable

        Comando.Parameters.AddWithValue("@IdCliente", IdCliente)

        Comando.CommandText = "SELECT * from CuentasCorrientes WHERE IdCliente = @IdCliente"

        Comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(Comando)
        Comando.Connection.Open()
        adaptador.Fill(dt)
        Comando.Connection.Close()

        Return dt

    End Function

    Public Function ObtenerDetalles() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Dim dt As New DataTable

        Comando.Parameters.AddWithValue("@IdCliente", IdCliente)

        Comando.CommandText = "SELECT dc.*,cc.Saldo from DetallesCuentas as dc " +
                                "inner join CuentasCorrientes as cc on cc.IdCuenta=dc.IdCuenta " +
                                "WHERE cc.IdCliente = @IdCliente"

        Comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(Comando)
        Comando.Connection.Open()
        adaptador.Fill(dt)
        Comando.Connection.Close()

        Return dt

    End Function

    Public Function CrearCuenta()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdCliente", IdCliente)
        Comando.Parameters.AddWithValue("@Saldo", Saldo)

        Comando.CommandText = "INSERT INTO CuentasCorrientes(IdCliente, Saldo) " +
                               "VALUES(@IdCliente, @Saldo)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()

        Dim id As Int64
        Comando.CommandText = "select @@identity"
        id = Comando.ExecuteScalar()

        Comando.Connection.Close()
        Return id

    End Function

    Public Sub CrearDetalle()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@Fecha", Fecha)
        Comando.Parameters.AddWithValue("@Monto", Monto)
        Comando.Parameters.AddWithValue("@Tipo", Tipo)
        Comando.Parameters.AddWithValue("@IdCuenta", IdCuenta)
        Comando.Parameters.AddWithValue("@IdVenta", IdVenta)

        Comando.CommandText = "INSERT INTO DetallesCuentas(Fecha, Monto, Tipo, IdCuenta, IdVenta) " +
                               "VALUES(@Fecha, @Monto, @Tipo, @IdCuenta, @IdVenta)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()

        Comando.Connection.Close()

    End Sub


    Public Sub DisminuirSaldo()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdCuenta", IdCuenta)
        Comando.Parameters.AddWithValue("@Monto", Monto)



        Comando.CommandText = "UPDATE CuentasCorrientes SET Saldo = Saldo - @Monto WHERE IdCuenta = @IdCuenta"


        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub IncrementarSaldo()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdCuenta", IdCuenta)
        Comando.Parameters.AddWithValue("@Monto", Monto)



        Comando.CommandText = "UPDATE CuentasCorrientes SET Saldo = Saldo + @Monto WHERE IdCuenta = @IdCuenta"


        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function BuscarCuentas(dt As DataTable, text As String) As Object
        Using adaptador As New SqlDataAdapter("Select cc.IdCliente, cc.IdCuenta , c.Documento, c.Apellido + ', ' + c.Nombre as Nombre, " +
                                              "cc.Saldo as Total from CuentasCorrientes cc inner join Clientes c on cc.IdCliente=c.IdCliente " +
                                                "where Activo = 1 And Documento  Like '" & "%" + text + "%" & "' " +
                                                "Or Nombre  Like '" & "%" + text + "%" & "' or Apellido  LIKE " +
                                                "'" & "%" + text + "%" & "' ORDER BY c.Apellido", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt
    End Function

End Class
