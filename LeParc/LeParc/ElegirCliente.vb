﻿Public Class ElegirCliente

    Dim DatosClientes As New DatosClientes
    Public b As Int64

    Private Sub ElegirCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Dim ind As New DatosClientes
        'Try
        '    DataGridView1.DataSource = ind.ObtenerTodo()
        '    DataGridView1.Columns("IdCliente").Visible = False
        '    DataGridView1.Columns("Activo").Visible = False
        '    DataGridView1.Columns("DirImagen").Visible = False
        '    DataGridView1.Columns("Email").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
        '    DataGridView1.Columns("Dni").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
        '    DataGridView1.ClearSelection()
        'Catch ex As Exception
        '    MessageBox.Show("Ocurrio un problema al cargar los clientes. Detalle: " & ex.Message.ToString)
        'End Try
    End Sub

    Private Sub bttSeleccionar_Click(sender As Object, e As EventArgs) Handles bttSeleccionar.Click
        Try
            If b = 0 Then

                Me.DialogResult = DialogResult.OK

                'NuevaVenta.bttElegirCliente.Text = DataGridView1.SelectedCells.Item(3).Value.ToString + ", " + DataGridView1.SelectedCells.Item(2).Value.ToString

                'NuevaVenta.idcliente = DataGridView1.SelectedCells.Item(0).Value.ToString

                'NuevaVenta.txtCodVenta.Focus()

            ElseIf b = 1 Then
                'Ventas.txtDNI.Text = DataGridView1.SelectedCells.Item(1).Value.ToString
            ElseIf b = 2 Then

            End If

            Me.Close()
        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al seleccionar el cliente. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs)

    End Sub

    Private Sub txtBuscar_KeyDown_1(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Dim cli As New DatosClientes
                Dim dt As New DataTable
                DataGridView1.DataSource = cli.buscar(dt, txtBuscar.Text)

                DataGridView1.Columns("IdCliente").Visible = False
                DataGridView1.Columns("Dirimagen").Visible = False
                DataGridView1.Columns("Activo").Visible = False

                DataGridView1.Columns("Documento").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                DataGridView1.Columns("Telefono").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader

            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrió un error al buscar el cliente. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave
        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If
    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        Dim f As New AgregarCliente

        Try
            If f.ShowDialog() = DialogResult.OK Then
                Dim cli As New DatosClientes
                Dim dt As New DataTable
                DataGridView1.DataSource = cli.buscar(dt, f.txtDocumento.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

End Class