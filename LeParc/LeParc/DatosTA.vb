﻿Imports System.Data.SqlClient
Public Class DatosTA
#Region "GetSet"
    Private IdTA As Int64
    Public Property IdTA_() As Int64
        Get
            Return IdTA
        End Get
        Set(ByVal value As Int64)
            IdTA = value
        End Set
    End Property

    Private Source As String
    Public Property Source_() As String
        Get
            Return Source
        End Get
        Set(ByVal value As String)
            Source = value
        End Set
    End Property

    Private Destination As String
    Public Property Destination_() As String
        Get
            Return Destination
        End Get
        Set(ByVal value As String)
            Destination = value
        End Set
    End Property

    Private GenerationTime As DateTime
    Public Property GenerationTime_() As DateTime
        Get
            Return GenerationTime
        End Get
        Set(ByVal value As DateTime)
            GenerationTime = value
        End Set
    End Property

    Private ExpirationTime As DateTime
    Public Property ExpirationTime_() As DateTime
        Get
            Return ExpirationTime
        End Get
        Set(ByVal value As DateTime)
            ExpirationTime = value
        End Set
    End Property

    Private Token As String
    Public Property Token_() As String
        Get
            Return Token
        End Get
        Set(ByVal value As String)
            Token = value
        End Set
    End Property

    Private Sign As String
    Public Property Sign_() As String
        Get
            Return Sign
        End Get
        Set(ByVal value As String)
            Sign = value
        End Set
    End Property

#End Region
    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

    Public Sub CrearTA()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@Source", Source)
        Comando.Parameters.AddWithValue("@Destination", Destination)
        Comando.Parameters.AddWithValue("@GenerationTime", GenerationTime)
        Comando.Parameters.AddWithValue("@ExpirationTime", ExpirationTime)
        Comando.Parameters.AddWithValue("@Token", Token)
        Comando.Parameters.AddWithValue("@Sign", Sign)

        Comando.CommandText = "INSERT INTO TicketsAcceso(Source,Destination,GenerationTime,ExpirationTime,Token,Sign) " +
                               "VALUES(@Source,@Destination,@GenerationTime,@ExpirationTime,@Token,@Sign)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerTA() As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        'comando.Parameters.AddWithValue("@fecha_desde", fd)
        'comando.Parameters.AddWithValue("@fecha_hasta", fh)

        comando.CommandText = "SELECT * from TicketsAcceso " +
        "WHERE ExpirationTime > GETDATE()"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function
End Class
