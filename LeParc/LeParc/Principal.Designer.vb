﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormPrincipal
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPrincipal))
        Me.PanelColor = New System.Windows.Forms.Panel()
        Me.PanelMenu = New System.Windows.Forms.Panel()
        Me.bttConfig = New System.Windows.Forms.Button()
        Me.bttSalir = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.bttInforme = New System.Windows.Forms.Button()
        Me.btnMantenimiento = New System.Windows.Forms.Button()
        Me.BttVentas = New System.Windows.Forms.Button()
        Me.BttAbout = New System.Windows.Forms.Button()
        Me.BttProductos = New System.Windows.Forms.Button()
        Me.BttClientes = New System.Windows.Forms.Button()
        Me.BttNuevaVenta = New System.Windows.Forms.Button()
        Me.bttFacturas = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PanelALL = New System.Windows.Forms.Panel()
        Me.bttAyuda = New System.Windows.Forms.Button()
        Me.PanelMenu.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelColor
        '
        Me.PanelColor.BackColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.PanelColor.Location = New System.Drawing.Point(3, 15)
        Me.PanelColor.Name = "PanelColor"
        Me.PanelColor.Size = New System.Drawing.Size(5, 50)
        Me.PanelColor.TabIndex = 26
        '
        'PanelMenu
        '
        Me.PanelMenu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PanelMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.PanelMenu.Controls.Add(Me.bttConfig)
        Me.PanelMenu.Controls.Add(Me.bttSalir)
        Me.PanelMenu.Controls.Add(Me.Panel2)
        Me.PanelMenu.Controls.Add(Me.PictureBox1)
        Me.PanelMenu.Location = New System.Drawing.Point(-3, -4)
        Me.PanelMenu.Name = "PanelMenu"
        Me.PanelMenu.Size = New System.Drawing.Size(239, 733)
        Me.PanelMenu.TabIndex = 27
        '
        'bttConfig
        '
        Me.bttConfig.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttConfig.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttConfig.FlatAppearance.BorderSize = 0
        Me.bttConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttConfig.Font = New System.Drawing.Font("Averta Regular", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttConfig.ForeColor = System.Drawing.Color.White
        Me.bttConfig.Image = Global.LeParc.My.Resources.Resources.settings
        Me.bttConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttConfig.Location = New System.Drawing.Point(3, 654)
        Me.bttConfig.Name = "bttConfig"
        Me.bttConfig.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttConfig.Size = New System.Drawing.Size(236, 30)
        Me.bttConfig.TabIndex = 40
        Me.bttConfig.Text = "    Configuración"
        Me.bttConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttConfig.UseVisualStyleBackColor = True
        '
        'bttSalir
        '
        Me.bttSalir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttSalir.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttSalir.FlatAppearance.BorderSize = 0
        Me.bttSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttSalir.Font = New System.Drawing.Font("Averta Regular", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttSalir.ForeColor = System.Drawing.Color.White
        Me.bttSalir.Image = Global.LeParc.My.Resources.Resources.close
        Me.bttSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttSalir.Location = New System.Drawing.Point(3, 687)
        Me.bttSalir.Name = "bttSalir"
        Me.bttSalir.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttSalir.Size = New System.Drawing.Size(236, 30)
        Me.bttSalir.TabIndex = 42
        Me.bttSalir.Text = "    Salir"
        Me.bttSalir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttSalir.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.Panel2.Controls.Add(Me.PanelColor)
        Me.Panel2.Controls.Add(Me.bttInforme)
        Me.Panel2.Controls.Add(Me.btnMantenimiento)
        Me.Panel2.Controls.Add(Me.BttVentas)
        Me.Panel2.Controls.Add(Me.BttAbout)
        Me.Panel2.Controls.Add(Me.BttProductos)
        Me.Panel2.Controls.Add(Me.BttClientes)
        Me.Panel2.Controls.Add(Me.BttNuevaVenta)
        Me.Panel2.Controls.Add(Me.bttFacturas)
        Me.Panel2.Location = New System.Drawing.Point(0, 156)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(239, 448)
        Me.Panel2.TabIndex = 33
        '
        'bttInforme
        '
        Me.bttInforme.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttInforme.FlatAppearance.BorderSize = 0
        Me.bttInforme.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttInforme.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttInforme.ForeColor = System.Drawing.Color.White
        Me.bttInforme.Image = Global.LeParc.My.Resources.Resources.informes1
        Me.bttInforme.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttInforme.Location = New System.Drawing.Point(3, 239)
        Me.bttInforme.Name = "bttInforme"
        Me.bttInforme.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttInforme.Size = New System.Drawing.Size(236, 50)
        Me.bttInforme.TabIndex = 4
        Me.bttInforme.Text = "   Informes"
        Me.bttInforme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttInforme.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttInforme.UseVisualStyleBackColor = True
        '
        'btnMantenimiento
        '
        Me.btnMantenimiento.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnMantenimiento.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.btnMantenimiento.FlatAppearance.BorderSize = 0
        Me.btnMantenimiento.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMantenimiento.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMantenimiento.ForeColor = System.Drawing.Color.White
        Me.btnMantenimiento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMantenimiento.Location = New System.Drawing.Point(3, 407)
        Me.btnMantenimiento.Name = "btnMantenimiento"
        Me.btnMantenimiento.Size = New System.Drawing.Size(236, 29)
        Me.btnMantenimiento.TabIndex = 5
        Me.btnMantenimiento.Text = "Mantenimiento"
        Me.btnMantenimiento.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMantenimiento.UseVisualStyleBackColor = False
        Me.btnMantenimiento.Visible = False
        '
        'BttVentas
        '
        Me.BttVentas.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.BttVentas.FlatAppearance.BorderSize = 0
        Me.BttVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BttVentas.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BttVentas.ForeColor = System.Drawing.Color.White
        Me.BttVentas.Image = Global.LeParc.My.Resources.Resources.ventas
        Me.BttVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttVentas.Location = New System.Drawing.Point(3, 183)
        Me.BttVentas.Name = "BttVentas"
        Me.BttVentas.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.BttVentas.Size = New System.Drawing.Size(236, 50)
        Me.BttVentas.TabIndex = 3
        Me.BttVentas.Text = "   Ventas"
        Me.BttVentas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttVentas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BttVentas.UseVisualStyleBackColor = True
        '
        'BttAbout
        '
        Me.BttAbout.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.BttAbout.FlatAppearance.BorderSize = 0
        Me.BttAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BttAbout.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BttAbout.ForeColor = System.Drawing.Color.White
        Me.BttAbout.Image = Global.LeParc.My.Resources.Resources.cuentas
        Me.BttAbout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttAbout.Location = New System.Drawing.Point(3, 295)
        Me.BttAbout.Name = "BttAbout"
        Me.BttAbout.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.BttAbout.Size = New System.Drawing.Size(236, 50)
        Me.BttAbout.TabIndex = 5
        Me.BttAbout.Text = "   Ctas. Corrientes"
        Me.BttAbout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BttAbout.UseVisualStyleBackColor = True
        '
        'BttProductos
        '
        Me.BttProductos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.BttProductos.FlatAppearance.BorderSize = 0
        Me.BttProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BttProductos.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BttProductos.ForeColor = System.Drawing.Color.White
        Me.BttProductos.Image = Global.LeParc.My.Resources.Resources.prod
        Me.BttProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttProductos.Location = New System.Drawing.Point(3, 71)
        Me.BttProductos.Name = "BttProductos"
        Me.BttProductos.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.BttProductos.Size = New System.Drawing.Size(236, 50)
        Me.BttProductos.TabIndex = 1
        Me.BttProductos.Text = "   Productos"
        Me.BttProductos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttProductos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BttProductos.UseVisualStyleBackColor = True
        '
        'BttClientes
        '
        Me.BttClientes.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.BttClientes.FlatAppearance.BorderSize = 0
        Me.BttClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BttClientes.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BttClientes.ForeColor = System.Drawing.Color.White
        Me.BttClientes.Image = Global.LeParc.My.Resources.Resources.clientes
        Me.BttClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttClientes.Location = New System.Drawing.Point(3, 127)
        Me.BttClientes.Name = "BttClientes"
        Me.BttClientes.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.BttClientes.Size = New System.Drawing.Size(236, 50)
        Me.BttClientes.TabIndex = 2
        Me.BttClientes.Text = "   Clientes"
        Me.BttClientes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttClientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BttClientes.UseVisualStyleBackColor = True
        '
        'BttNuevaVenta
        '
        Me.BttNuevaVenta.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.BttNuevaVenta.FlatAppearance.BorderSize = 0
        Me.BttNuevaVenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BttNuevaVenta.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BttNuevaVenta.ForeColor = System.Drawing.Color.White
        Me.BttNuevaVenta.Image = Global.LeParc.My.Resources.Resources.nuevaventa1
        Me.BttNuevaVenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttNuevaVenta.Location = New System.Drawing.Point(3, 15)
        Me.BttNuevaVenta.Name = "BttNuevaVenta"
        Me.BttNuevaVenta.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.BttNuevaVenta.Size = New System.Drawing.Size(236, 50)
        Me.BttNuevaVenta.TabIndex = 0
        Me.BttNuevaVenta.Text = "   Nueva Venta"
        Me.BttNuevaVenta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BttNuevaVenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BttNuevaVenta.UseVisualStyleBackColor = True
        '
        'bttFacturas
        '
        Me.bttFacturas.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.bttFacturas.FlatAppearance.BorderSize = 0
        Me.bttFacturas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttFacturas.Font = New System.Drawing.Font("Averta Regular", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttFacturas.ForeColor = System.Drawing.Color.White
        Me.bttFacturas.Image = Global.LeParc.My.Resources.Resources.facturas
        Me.bttFacturas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttFacturas.Location = New System.Drawing.Point(3, 351)
        Me.bttFacturas.Name = "bttFacturas"
        Me.bttFacturas.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.bttFacturas.Size = New System.Drawing.Size(236, 50)
        Me.bttFacturas.TabIndex = 27
        Me.bttFacturas.Text = "   Facturas"
        Me.bttFacturas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bttFacturas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttFacturas.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.LeParc.My.Resources.Resources.logo1
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.Location = New System.Drawing.Point(36, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(166, 98)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'ToolTip1
        '
        Me.ToolTip1.AutomaticDelay = 0
        Me.ToolTip1.AutoPopDelay = 3220
        Me.ToolTip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ToolTip1.ForeColor = System.Drawing.Color.White
        Me.ToolTip1.InitialDelay = 100
        Me.ToolTip1.ReshowDelay = 64
        '
        'PanelALL
        '
        Me.PanelALL.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelALL.Location = New System.Drawing.Point(264, 93)
        Me.PanelALL.Name = "PanelALL"
        Me.PanelALL.Size = New System.Drawing.Size(1000, 594)
        Me.PanelALL.TabIndex = 41
        '
        'bttAyuda
        '
        Me.bttAyuda.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttAyuda.BackgroundImage = Global.LeParc.My.Resources.Resources.help
        Me.bttAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.bttAyuda.FlatAppearance.BorderSize = 0
        Me.bttAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttAyuda.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttAyuda.Location = New System.Drawing.Point(1229, 51)
        Me.bttAyuda.Name = "bttAyuda"
        Me.bttAyuda.Size = New System.Drawing.Size(35, 35)
        Me.bttAyuda.TabIndex = 45
        Me.bttAyuda.UseVisualStyleBackColor = True
        '
        'FormPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1296, 728)
        Me.Controls.Add(Me.bttAyuda)
        Me.Controls.Add(Me.PanelMenu)
        Me.Controls.Add(Me.PanelALL)
        Me.ForeColor = System.Drawing.Color.Black
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(1296, 728)
        Me.Name = "FormPrincipal"
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.White
        Me.Tag = ""
        Me.Text = "Le Parc Soft"
        Me.PanelMenu.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BttProductos As Button
    Friend WithEvents bttInforme As Button
    Friend WithEvents BttVentas As Button
    Friend WithEvents BttClientes As Button
    Friend WithEvents PanelColor As Panel
    Friend WithEvents PanelMenu As Panel
    Friend WithEvents BttAbout As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents BttNuevaVenta As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents PanelALL As Panel
    Friend WithEvents btnMantenimiento As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents bttConfig As Button
    Friend WithEvents bttSalir As Button
    Friend WithEvents bttAyuda As Button
    Friend WithEvents bttFacturas As Button
End Class
