﻿Public Class ConfigTalles

    Dim opcion As Integer

    Private Sub ConfigTalles_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim talle As New DatosTalles
        dgvTalles.DataSource = talle.ObtenerTodo
        dgvTalles.Columns("IdTalle").Visible = False
        dgvTalles.Columns("Activo").Visible = False
        dgvTalles.Columns("Nombre").HeaderText = "Talle"

    End Sub

    Private Sub dgvMarcas_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTalles.CellClick

        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtTalle.Text = dgvTalles.CurrentRow.Cells("Nombre").Value
        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        ' Agregar
        txtTalle.Enabled = True
        txtTalle.Clear()
        txtTalle.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvTalles.Enabled = False

        opcion = 1

    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click

        ' Modificar
        txtTalle.Enabled = True
        txtTalle.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvTalles.Enabled = False

        opcion = 2

    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este registro?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                Dim talle As New DatosTalles
                talle.IdTalle_ = dgvTalles.SelectedRows(0).Cells("IdTalle").Value
                talle.EliminarTalle()
                MsgBox("Se eliminó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvTalles.Enabled = True
                dgvTalles.DataSource = talle.ObtenerTodo()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el registro. Detalle:" & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        dgvTalles.Enabled = True
        dgvTalles.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtTalle.Enabled = False
        txtTalle.Clear()

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtTalle.Text <> "" Then

            Dim talle As New DatosTalles

            If opcion = 1 Then 'AGREGAR
                Try
                    talle.Nombre_ = txtTalle.Text
                    talle.InsertarTalle()
                    MsgBox("Registro creado con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvTalles.Enabled = True
                    dgvTalles.DataSource = talle.ObtenerTodo()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el registro. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try

                    talle.Nombre_ = txtTalle.Text
                    talle.IdTalle_ = dgvTalles.SelectedRows(0).Cells("IdTalle").Value
                    talle.ModificarTalle()
                    MsgBox("Se modificó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvTalles.Enabled = True
                    dgvTalles.DataSource = talle.ObtenerTodo

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el registro. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If

    End Sub

End Class