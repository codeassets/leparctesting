﻿Imports System.Data.SqlClient

Public Class DatosMigracion

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property


#Region "GetSet"

    Private IdVentas As Long
    Public Property IdVentas_() As Long
        Get
            Return IdVentas
        End Get
        Set(ByVal value As Long)
            IdVentas = value
        End Set
    End Property

    Private IdCliente As Integer
    Public Property IdCliente_() As Integer
        Get
            Return IdCliente
        End Get
        Set(ByVal value As Integer)
            IdCliente = value
        End Set
    End Property

    Private IdProducto As Integer
    Public Property IdProducto_() As Integer
        Get
            Return IdProducto
        End Get
        Set(ByVal value As Integer)
            IdProducto = value
        End Set
    End Property

    Private Fecha As Date
    Public Property Fecha_() As Date
        Get
            Return Fecha
        End Get
        Set(ByVal value As Date)
            Fecha = value
        End Set
    End Property

    Private FechaDesde As Date
    Public Property FechaDesde_() As Date
        Get
            Return FechaDesde
        End Get
        Set(ByVal value As Date)
            FechaDesde = value
        End Set
    End Property

    Private FechaHasta As Date
    Public Property FechaHasta_() As Date
        Get
            Return FechaHasta
        End Get
        Set(ByVal value As Date)
            FechaHasta = value
        End Set
    End Property

    Private Cantidad As Integer
    Public Property Cantidad_() As Integer
        Get
            Return Cantidad
        End Get
        Set(ByVal value As Integer)
            Cantidad = value
        End Set
    End Property

    Private Total As Double
    Public Property Total_() As Double
        Get
            Return Total
        End Get
        Set(ByVal value As Double)
            Total = value
        End Set
    End Property

    Private Documento As Integer
    Public Property Documento_() As Integer
        Get
            Return Documento
        End Get
        Set(ByVal value As Integer)
            Documento = value
        End Set
    End Property

    Private Pago As String
    Public Property Pago_() As String
        Get
            Return Pago
        End Get
        Set(ByVal value As String)
            Pago = value
        End Set
    End Property

    Private Tipo As String
    Public Property Tipo_() As String
        Get
            Return Tipo
        End Get
        Set(ByVal value As String)
            Tipo = value
        End Set
    End Property

    Private Descuento As Integer
    Public Property Descuento_() As Integer
        Get
            Return Descuento
        End Get
        Set(ByVal value As Integer)
            Descuento = value
        End Set
    End Property

    Private IdUsuario As Long
    Public Property IdUsuario_() As Long
        Get
            Return IdUsuario
        End Get
        Set(ByVal value As Long)
            IdUsuario = value
        End Set
    End Property

    Private IdDetalle As Long
    Public Property IdDetalle_() As Long
        Get
            Return IdDetalle
        End Get
        Set(ByVal value As Long)
            IdDetalle = value
        End Set
    End Property

    Private Descripcion As String
    Public Property Descripcion_() As String
        Get
            Return Descripcion
        End Get
        Set(ByVal value As String)
            Descripcion = value
        End Set
    End Property

    Private Marca As String
    Public Property Marca_() As String
        Get
            Return Marca
        End Get
        Set(ByVal value As String)
            Marca = value
        End Set
    End Property

    Private Color As String
    Public Property Color_() As String
        Get
            Return Color
        End Get
        Set(ByVal value As String)
            Color = value
        End Set
    End Property

    Private Talle As String
    Public Property Talle_() As String
        Get
            Return Talle
        End Get
        Set(ByVal value As String)
            Talle = value
        End Set
    End Property

    Private PrecioUnitario As Double
    Public Property PrecioUnitario_() As Double
        Get
            Return PrecioUnitario
        End Get
        Set(ByVal value As Double)
            PrecioUnitario = value
        End Set
    End Property

    Private Subtotal As Double
    Public Property Subtotal_() As Double
        Get
            Return Subtotal
        End Get
        Set(ByVal value As Double)
            Subtotal = value
        End Set
    End Property



#End Region

    Public Function ObtenerTodoVentasViejas() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "SELECT v.*,p.IdProducto,p.Descripcion,p.Marca,p.Color,p.Talle FROM Ventas v " +
        "left join Productos p on v.IdProducto=p.IdProducto " +
        "where v.Activo = 1 order by v.Fecha desc"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Function InsertaVentas()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdCliente", IdCliente)
        Comando.Parameters.AddWithValue("@Fecha", Fecha)
        Comando.Parameters.AddWithValue("@Tipo", Tipo)
        Comando.Parameters.AddWithValue("@Pago", Pago)
        Comando.Parameters.AddWithValue("@Descuento", Descuento)
        Comando.Parameters.AddWithValue("@Total", Total)
        Comando.Parameters.AddWithValue("@IdUsuario", IdUsuario)

        Comando.CommandText = "INSERT INTO Ventas2(IdCliente, Fecha, Tipo, Pago, Descuento, Total, IdUsuario) " +
                               "VALUES(@IdCliente, @Fecha, @Tipo, @Pago, @Descuento, @Total, @IdUsuario)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()

        Dim id As Int64
        Comando.CommandText = "SELECT IDENT_CURRENT('Ventas2')"
        id = Comando.ExecuteScalar()

        Comando.Connection.Close()
        Return id

    End Function

    Public Sub CrearDetalleVenta()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdProducto", IdProducto)
        Comando.Parameters.AddWithValue("@Descripcion", Descripcion)
        Comando.Parameters.AddWithValue("@Marca", Marca)
        Comando.Parameters.AddWithValue("@Color", Color)
        Comando.Parameters.AddWithValue("@Talle", Talle)
        Comando.Parameters.AddWithValue("@Cantidad", Cantidad)
        Comando.Parameters.AddWithValue("@PrecioUnitario", PrecioUnitario)
        Comando.Parameters.AddWithValue("@Subtotal", Subtotal)
        Comando.Parameters.AddWithValue("@IdVenta", IdVentas)

        Comando.CommandText = "INSERT INTO DetallesVentas(IdProducto, Descripcion, Marca, Color, Talle, Cantidad, PrecioUnitario, Subtotal, IdVenta) " +
                               "VALUES(@IdProducto, @Descripcion, @Marca, @Color, @Talle, @Cantidad, @PrecioUnitario, @Subtotal, @IdVenta)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerVentasNuevas() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "SELECT * FROM Ventas2 where Activo = 1"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Function ObtenerDetalles() As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@IdVenta", IdVentas)

        comando.CommandText = "select d.IdProducto,d.Descripcion, d.Marca, d.Color, p.Color as CodigoColor, d.Talle, d.Cantidad, d.PrecioUnitario, d.Subtotal, v.Total " +
        "from DetallesVentas as d inner join Productos as p on d.IdProducto = p.IdProducto inner join Ventas2 as v on d.IdVenta = v.IdVenta " +
        "WHERE d.IdVenta = @IdVenta And d.Activo = 1"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function



End Class
