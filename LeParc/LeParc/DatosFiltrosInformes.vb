﻿Imports System.Data.SqlClient

Public Class DatosFiltrosInformes

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

#Region "GetSet"

    Private IdFiltro As Int64
    Public Property IdFiltro_() As Int64
        Get
            Return IdFiltro
        End Get
        Set(ByVal value As Int64)
            IdFiltro = value
        End Set
    End Property

    Private Nombre As String
    Public Property Nombre_() As String
        Get
            Return Nombre
        End Get
        Set(ByVal value As String)
            Nombre = value
        End Set
    End Property

#End Region


    Public Sub InsertarFiltro()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Comando.Parameters.AddWithValue("@Nombre", Nombre)
        Comando.CommandText = "INSERT INTO FiltrosInformes(Nombre) " +
                               "VALUES(@Nombre)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ModificarFiltro()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@IdFiltro", IdFiltro)
        Comando.Parameters.AddWithValue("@Nombre", Nombre)

        Comando.CommandText = "UPDATE FiltrosInformes SET Nombre = @Nombre WHERE IdFiltro = @IdFiltro"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub EliminarFiltro()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdFiltro", IdFiltro)
        Comando.CommandText = "UPDATE FiltrosInformes SET Activo = 0 WHERE IdFiltro = @IdFiltro"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerTodo() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT * from FiltrosInformes WHERE Activo = 1 ORDER BY Nombre ASC", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

End Class
