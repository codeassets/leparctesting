﻿Imports System.Data.SqlClient

Public Class DatosVentas

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property


#Region "GetSet"

    Private IdVentas As Long
    Public Property IdVentas_() As Long
        Get
            Return IdVentas
        End Get
        Set(ByVal value As Long)
            IdVentas = value
        End Set
    End Property

    Private IdCliente As Integer
    Public Property IdCliente_() As Integer
        Get
            Return IdCliente
        End Get
        Set(ByVal value As Integer)
            IdCliente = value
        End Set
    End Property

    Private IdProducto As Integer
    Public Property IdProducto_() As Integer
        Get
            Return IdProducto
        End Get
        Set(ByVal value As Integer)
            IdProducto = value
        End Set
    End Property

    Private Fecha As Date
    Public Property Fecha_() As Date
        Get
            Return Fecha
        End Get
        Set(ByVal value As Date)
            Fecha = value
        End Set
    End Property

    Private FechaDesde As Date
    Public Property FechaDesde_() As Date
        Get
            Return FechaDesde
        End Get
        Set(ByVal value As Date)
            FechaDesde = value
        End Set
    End Property

    Private FechaHasta As Date
    Public Property FechaHasta_() As Date
        Get
            Return FechaHasta
        End Get
        Set(ByVal value As Date)
            FechaHasta = value
        End Set
    End Property

    Private Cantidad As Integer
    Public Property Cantidad_() As Integer
        Get
            Return Cantidad
        End Get
        Set(ByVal value As Integer)
            Cantidad = value
        End Set
    End Property

    Private Total As Double
    Public Property Total_() As Double
        Get
            Return Total
        End Get
        Set(ByVal value As Double)
            Total = value
        End Set
    End Property

    Private Documento As Int64
    Public Property Documento_() As Int64
        Get
            Return Documento
        End Get
        Set(ByVal value As Int64)
            Documento = value
        End Set
    End Property

    Private Pago As String
    Public Property Pago_() As String
        Get
            Return Pago
        End Get
        Set(ByVal value As String)
            Pago = value
        End Set
    End Property

    Private Tipo As String
    Public Property Tipo_() As String
        Get
            Return Tipo
        End Get
        Set(ByVal value As String)
            Tipo = value
        End Set
    End Property

    Private Descuento As Integer
    Public Property Descuento_() As Integer
        Get
            Return Descuento
        End Get
        Set(ByVal value As Integer)
            Descuento = value
        End Set
    End Property

    Private IdUsuario As Long
    Public Property IdUsuario_() As Long
        Get
            Return IdUsuario
        End Get
        Set(ByVal value As Long)
            IdUsuario = value
        End Set
    End Property

    Private IdDetalle As Long
    Public Property IdDetalle_() As Long
        Get
            Return IdDetalle
        End Get
        Set(ByVal value As Long)
            IdDetalle = value
        End Set
    End Property

    Private Descripcion As String
    Public Property Descripcion_() As String
        Get
            Return Descripcion
        End Get
        Set(ByVal value As String)
            Descripcion = value
        End Set
    End Property

    Private Marca As String
    Public Property Marca_() As String
        Get
            Return Marca
        End Get
        Set(ByVal value As String)
            Marca = value
        End Set
    End Property

    Private Color As String
    Public Property Color_() As String
        Get
            Return Color
        End Get
        Set(ByVal value As String)
            Color = value
        End Set
    End Property

    Private Talle As String
    Public Property Talle_() As String
        Get
            Return Talle
        End Get
        Set(ByVal value As String)
            Talle = value
        End Set
    End Property

    Private PrecioUnitario As Double
    Public Property PrecioUnitario_() As Double
        Get
            Return PrecioUnitario
        End Get
        Set(ByVal value As Double)
            PrecioUnitario = value
        End Set
    End Property

    Private Subtotal As Double
    Public Property Subtotal_() As Double
        Get
            Return Subtotal
        End Get
        Set(ByVal value As Double)
            Subtotal = value
        End Set
    End Property
    Private IdVendedor As Nullable(Of Int64)
    Public Property IdVendedor_() As Nullable(Of Int64)
        Get
            Return IdVendedor
        End Get
        Set(ByVal value As Nullable(Of Int64))
            IdVendedor = value
        End Set
    End Property



#End Region

    Public Function ObtenerTodo() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "SELECT * FROM Ventas2 where Activo = 1"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Function BuscarVenta(fd As Date, fh As Date) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        Comando.Parameters.AddWithValue("@FechaDesde", fd)
        Comando.Parameters.AddWithValue("@FechaHasta", fh)

        comando.CommandText = "select v.IdVenta, v.Fecha, c.Documento, v.Tipo, v.Pago, v.Descuento, v.Total, v.Activo, vd.Nombre as Vendedor from Ventas2 as v " +
        "left join Clientes as c on v.IdCliente = c.IdCliente left join Vendedores as vd on v.IdVendedor = vd.IdVendedor " +
        "WHERE v.Fecha >= @FechaDesde AND v.Fecha <= @FechaHasta"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(Comando)
        Comando.Connection.Open()
        adaptador.Fill(dt)
        Comando.Connection.Close()
        Return dt

    End Function

    Public Function ObtenerDetalles() As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@IdVenta", IdVentas)

        comando.CommandText = "select d.IdDetalle, d.IdProducto,d.Descripcion, d.Marca, d.Color, p.Color as CodigoColor, d.Talle, d.Cantidad, d.PrecioUnitario, d.Subtotal, v.Total, v.IdVenta " +
        "from DetallesVentas as d inner join Productos as p on d.IdProducto = p.IdProducto inner join Ventas2 as v on d.IdVenta = v.IdVenta " +
        "WHERE d.IdVenta = @IdVenta And d.Activo = 1"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function BuscarPorMesyAño(mes As Integer, año As Integer) As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Comando.Parameters.AddWithValue("@mes", mes)
        Comando.Parameters.AddWithValue("@año", año)

        Accion = "select v.IdVenta, v.Fecha, c.Documento, v.Tipo, v.Pago, v.Descuento, v.Total, v.Activo, vd.Nombre as Vendedor from Ventas2 as v " +
        "left join Clientes as c on v.IdCliente = c.IdCliente left join Vendedores as vd on v.IdVendedor = vd.IdVendedor WHERE MONTH(v.Fecha) = '" & mes & "' " +
        "And YEAR(v.Fecha) = '" & año & "' order by Fecha desc"

        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Function BuscarFechaDesdeHasta(fd As Date, fh As Date) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@FechaDesde", fd)
        comando.Parameters.AddWithValue("@FechaHasta", fh)

        comando.CommandText = "select p.Color,v.IdVenta, v.Fecha,c.Apellido + ', ' + c.Nombre as Cliente, p.Descripcion + ' - ' + p.Marca + ' - ' + p.Talle + ' - ' + p.NombreColor as Producto,v.cantidad, v.importe, v.Tipo, v.Pago, v.Descuento " +
        "from Ventas as v inner join Productos as p on v.IdProducto = p.IdProducto left join Clientes c on v.IdCliente=c.IdCliente " +
        "WHERE v.Fecha >= @FechaDesde And v.Fecha <=@FechaHasta And v.Activo = 1 order by v.Fecha"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function InformeVentasPorFechas(fd As Date, fh As Date) As DataTable

        Dim dt As New DataTable
        Dim comando As New SqlCommand

        comando.Parameters.AddWithValue("@FechaDesde", fd)
        comando.Parameters.AddWithValue("@FechaHasta", fh)

        comando.CommandText = "SELECT v.IdVenta, v.Fecha, c.Documento, v.Tipo, v.Pago, v.Descuento, v.Total from Ventas2 v " +
        "left join Clientes c on v.IdCliente = c.IdCliente WHERE v.Fecha >= @FechaDesde And v.Fecha <=@FechaHasta And v.Activo = 1 order by v.Fecha"

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()

        Return dt

    End Function

    Public Function BuscarxCliente() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Dim dt As New DataTable

        Comando.Parameters.AddWithValue("@Documento", Documento)

        Comando.CommandText = "select v.IdVenta, v.Fecha, c.Documento, v.Tipo, v.Pago, v.Descuento, v.Total, v.Activo, vd.Nombre as Vendedor from Ventas2 as v " +
        "left join Clientes as c on v.IdCliente = c.IdCliente left join Vendedores as vd on v.IdVendedor = vd.IdVendedor " +
        "WHERE Documento = @Documento"

        Comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(Comando)
        Comando.Connection.Open()
        adaptador.Fill(dt)
        Comando.Connection.Close()

        Return dt

    End Function

    Public Function InsertaVentas()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdCliente", IdCliente)
        Comando.Parameters.AddWithValue("@Fecha", Fecha)
        Comando.Parameters.AddWithValue("@Tipo", Tipo)
        Comando.Parameters.AddWithValue("@Pago", Pago)
        Comando.Parameters.AddWithValue("@Descuento", Descuento)
        Comando.Parameters.AddWithValue("@Total", Total)
        Comando.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        Comando.Parameters.AddWithValue("@IdVendedor", If(IdVendedor, DBNull.Value))

        Comando.CommandText = "INSERT INTO Ventas2(IdCliente, Fecha, Tipo, Pago, Descuento, Total, IdUsuario, IdVendedor) " +
                               "VALUES(@IdCliente, @Fecha, @Tipo, @Pago, @Descuento, @Total, @IdUsuario,@IdVendedor)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()

        Dim id As Int64
        Comando.CommandText = "select @@identity"
        id = Comando.ExecuteScalar()

        Comando.Connection.Close()
        Return id

    End Function

    Public Sub CrearDetalleVenta()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdProducto", IdProducto)
        Comando.Parameters.AddWithValue("@Descripcion", Descripcion)
        Comando.Parameters.AddWithValue("@Marca", Marca)
        Comando.Parameters.AddWithValue("@Color", Color)
        Comando.Parameters.AddWithValue("@Talle", Talle)
        Comando.Parameters.AddWithValue("@Cantidad", Cantidad)
        Comando.Parameters.AddWithValue("@PrecioUnitario", PrecioUnitario)
        Comando.Parameters.AddWithValue("@Subtotal", Subtotal)
        Comando.Parameters.AddWithValue("@IdVenta", IdVentas)

        Comando.CommandText = "INSERT INTO DetallesVentas(IdProducto, Descripcion, Marca, Color, Talle, Cantidad, PrecioUnitario, Subtotal, IdVenta) " +
                               "VALUES(@IdProducto, @Descripcion, @Marca, @Color, @Talle, @Cantidad, @PrecioUnitario, @Subtotal, @IdVenta)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ModificarVenta()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdCliente", IdCliente)
        Comando.Parameters.AddWithValue("@IdProducto", IdProducto)
        Comando.Parameters.AddWithValue("@Fecha", Fecha)
        Comando.Parameters.AddWithValue("@Cantidad", Cantidad)
        Comando.Parameters.AddWithValue("@Importe", Total)
        Comando.Parameters.AddWithValue("@Tipo", Tipo)
        Comando.Parameters.AddWithValue("@Pago", Pago)
        Comando.Parameters.AddWithValue("@Descuento", Descuento)

        Comando.CommandText = "UPDATE Venta SET IdCliente=@IdCliente, IdProducto = @IdProducto," +
        "Fecha = @Fecha, Cantidad = @Cantidad, Importe = @Importe, Tipo = @Tipo, Pago = @Pago, Descuento = @Descuento" +
        " WHERE IdVenta = @IdVenta"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ActualizarTotalVenta()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdVenta", IdVentas)
        Comando.Parameters.AddWithValue("@Total", Total)

        Comando.CommandText = "UPDATE Ventas2 SET Total = @Total WHERE IdVenta = @IdVenta"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ActualizarCantidadDetalle()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdDetalle", IdDetalle)
        Comando.Parameters.AddWithValue("@Cantidad", Cantidad)
        Comando.Parameters.AddWithValue("@Subtotal", Subtotal)

        Comando.CommandText = "UPDATE DetallesVentas SET Cantidad = Cantidad - @Cantidad, Subtotal = Subtotal - @Subtotal WHERE IdDetalle = @IdDetalle"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ElimiarDetalle()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdDetalle", IdDetalle)

        Comando.CommandText = "DELETE from DetallesVentas WHERE IdDetalle = @IdDetalle"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub AnularVenta()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdVenta", IdVentas)

        Comando.CommandText = "UPDATE Ventas2 SET Activo = 0 WHERE IdVenta = @IdVenta"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub


End Class
