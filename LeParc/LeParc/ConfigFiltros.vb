﻿Public Class ConfigFiltros
    Dim opcion As Integer

    Private Sub ConfigFiltros_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim filtro As New DatosFiltrosInformes
        dgvFiltros.DataSource = filtro.ObtenerTodo
        dgvFiltros.Columns("IdFiltro").Visible = False
        dgvFiltros.Columns("Activo").Visible = False
        dgvFiltros.Columns("Nombre").HeaderText = "Filtro"

    End Sub

    Private Sub dgvMarcas_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvFiltros.CellClick

        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtFiltro.Text = dgvFiltros.CurrentRow.Cells("Nombre").Value
        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        ' Agregar
        txtFiltro.Enabled = True
        txtFiltro.Clear()
        txtFiltro.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvFiltros.Enabled = False

        opcion = 1

    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click

        ' Modificar
        txtFiltro.Enabled = True
        txtFiltro.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvFiltros.Enabled = False

        opcion = 2

    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este filtro?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                Dim filtro As New DatosFiltrosInformes
                filtro.IdFiltro_ = dgvFiltros.SelectedRows(0).Cells("IdFiltro").Value
                filtro.EliminarFiltro()
                MsgBox("Se eliminó el filtro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvFiltros.Enabled = True
                dgvFiltros.DataSource = filtro.ObtenerTodo()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el registro. Detalle:" & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click

        dgvFiltros.Enabled = True
        dgvFiltros.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtFiltro.Enabled = False
        txtFiltro.Clear()

    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtFiltro.Text <> "" Then

            Dim filtro As New DatosFiltrosInformes

            If opcion = 1 Then 'AGREGAR
                Try
                    filtro.Nombre_ = txtFiltro.Text
                    filtro.InsertarFiltro()
                    MsgBox("Registro creado con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvFiltros.Enabled = True
                    dgvFiltros.DataSource = filtro.ObtenerTodo()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el registro. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try

                    filtro.Nombre_ = txtFiltro.Text
                    filtro.IdFiltro_ = dgvFiltros.SelectedRows(0).Cells("IdFiltro").Value
                    filtro.ModificarFiltro()
                    MsgBox("Se modificó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvFiltros.Enabled = True
                    dgvFiltros.DataSource = filtro.ObtenerTodo

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el registro. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If

    End Sub
End Class