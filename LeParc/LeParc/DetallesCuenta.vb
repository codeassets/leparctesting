﻿Public Class DetallesCuenta

    Dim IdVenta As Long

    Sub New(_id As Long)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        IdVenta = _id

    End Sub

    Private Function ContrastColor(ByVal color As Color) As Color
        Dim d As Integer = 0
        Dim luminance As Double = (0.299 * color.R + 0.587 * color.G + 0.114 * color.B) / 255

        If luminance > 0.5 Then
            d = 0
        Else
            d = 255
        End If

        Return Color.FromArgb(d, d, d)
    End Function

    Private Sub DetallesCuenta_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        Dim venta As New DatosVentas

        venta.IdVentas_ = IdVenta
        dgvDetalles.DataSource = venta.ObtenerDetalles()
        dgvDetalles.Columns("Total").Visible = False
        dgvDetalles.Columns("IdDetalle").Visible = False
        dgvDetalles.Columns("IdProducto").Visible = False
        dgvDetalles.Columns("CodigoColor").Visible = False
        dgvDetalles.Columns("IdVenta").Visible = False
        dgvDetalles.Columns("Descripcion").HeaderText = "Producto"
        dgvDetalles.Columns("PrecioUnitario").HeaderText = "Precio Unit."

        lblTotal.Text = "Total: $ " & dgvDetalles.Rows(0).Cells("Total").Value

    End Sub

    Private Sub dgvDetalles_SelectionChanged(sender As Object, e As EventArgs) Handles dgvDetalles.SelectionChanged
        If dgvDetalles.SelectedRows.Count <> 0 Then
            dgvDetalles.RowsDefaultCellStyle.SelectionBackColor = Color.FromArgb(Convert.ToInt32(dgvDetalles.CurrentRow.Cells("CodigoColor").Value))
            Dim forecolor As Color = ContrastColor(Color.FromArgb(Convert.ToInt32(dgvDetalles.CurrentRow.Cells("CodigoColor").Value)))
            dgvDetalles.RowsDefaultCellStyle.SelectionForeColor = forecolor
        End If
    End Sub

End Class