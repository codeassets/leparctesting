﻿Public Class CuentasCorrientes

    Dim idColumnaSeleccionada As Integer

    'Funcion para que no se coloque mas de una coma en el txtbox
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt

    End Function

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim cli As New DatosCuentasCorrientes
            Dim dt As New DataTable
            Try
                dgvClientes.DataSource = cli.BuscarCuentas(dt, txtBuscar.Text)
                dgvClientes.Columns("IdCliente").Visible = False
                dgvClientes.Columns("IdCuenta").Visible = False
                dgvClientes.Columns("Documento").FillWeight = 30
                dgvClientes.Columns("Nombre").FillWeight = 40
                dgvClientes.Columns("Total").FillWeight = 30
                dgvClientes.ClearSelection()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al cargar la lista de clientes. Detalle:" & ex.Message.ToString)
            End Try

            If dgvClientes.RowCount > 0 Then
                bttCancelarSeleccion.Enabled = True
            Else
                bttCancelarSeleccion.Enabled = False
            End If

        End If
    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave
        If Trim(txtBuscar.Text) = "" Then
            'txtBuscar.Text = "Presione ENTER para buscar"
        End If
    End Sub

    Private Sub dgvClientes_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvClientes.CellMouseDoubleClick

        bttCrearCuenta.Enabled = False

        Try

            idColumnaSeleccionada = dgvClientes.SelectedRows(0).Cells("IdCuenta").Value

            Dim cuenta As New DatosCuentasCorrientes
            cuenta.IdCliente_ = dgvClientes.SelectedRows(0).Cells("IdCliente").Value
            dgvMovimientos.DataSource = cuenta.ObtenerDetalles()
            dgvMovimientos.Columns("IdDetalle").Visible = False
            dgvMovimientos.Columns("IdCuenta").Visible = False
            dgvMovimientos.Columns("Saldo").Visible = False
            dgvMovimientos.Columns("IdVenta").Visible = False
            dgvMovimientos.Columns("Fecha").FillWeight = 40
            dgvMovimientos.Columns("Monto").FillWeight = 30
            dgvMovimientos.Columns("Tipo").FillWeight = 30

            lblCliente.Text = "Cliente: " & dgvClientes.SelectedRows(0).Cells("Documento").Value & " - " & dgvClientes.SelectedRows(0).Cells("Nombre").Value

            If dgvMovimientos.RowCount > 0 Then
                lblTotal.Text = "Total adeudado: $" & dgvMovimientos.SelectedRows(0).Cells("Saldo").Value
            End If

            dgvClientes.Enabled = False

            For Each row As DataGridViewRow In dgvClientes.Rows
                row.DefaultCellStyle.BackColor = Color.Gray
            Next

            For Each row As DataGridViewRow In dgvMovimientos.Rows
                If row.Cells("Tipo").Value = "Pago" Then
                    row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
                ElseIf row.Cells("Tipo").Value = "Compra" Then
                    row.DefaultCellStyle.BackColor = Color.Salmon
                End If
            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttPagar_Click(sender As Object, e As EventArgs) Handles bttPagar.Click

        Try
            If Trim(txtMonto.Text) <> "" And txtMonto.Text <> 0 Then

                If txtMonto.Text <= dgvClientes.SelectedRows(0).Cells("Total").Value Then

                    If dgvClientes.SelectedRows(0).Cells("Total").Value <> 0 Then

                        Try
                            Dim detalle As New DatosCuentasCorrientes
                            detalle.Fecha_ = dtpFecha.Value
                            detalle.Monto_ = txtMonto.Text
                            detalle.Tipo_ = "Pago"
                            detalle.IdCuenta_ = dgvClientes.SelectedRows(0).Cells("IdCuenta").Value
                            detalle.CrearDetalle()
                            detalle.DisminuirSaldo()

                            detalle.IdCliente_ = dgvClientes.SelectedRows(0).Cells("IdCliente").Value
                            dgvMovimientos.DataSource = detalle.ObtenerDetalles()
                            dgvMovimientos.Columns("IdDetalle").Visible = False
                            dgvMovimientos.Columns("IdCuenta").Visible = False
                            dgvMovimientos.Columns("Saldo").Visible = False
                            dgvMovimientos.Columns("Fecha").FillWeight = 40
                            dgvMovimientos.Columns("Monto").FillWeight = 30
                            dgvMovimientos.Columns("Tipo").FillWeight = 30

                            lblTotal.Text = "Total adeudado: $" & dgvMovimientos.SelectedRows(0).Cells("Saldo").Value
                            txtMonto.Text = ""
                            dtpFecha.Value = Date.Now


#Region "Actualizar tabla clientes y seleccionar la fila"
                            ' Actualizar tabla clientes
                            Dim cli As New DatosCuentasCorrientes
                            Dim dt As New DataTable
                            Try
                                dgvClientes.DataSource = cli.BuscarCuentas(dt, txtBuscar.Text)
                                dgvClientes.Columns("IdCliente").Visible = False
                                dgvClientes.Columns("IdCuenta").Visible = False
                                dgvClientes.Columns("Documento").FillWeight = 30
                                dgvClientes.Columns("Nombre").FillWeight = 40
                                dgvClientes.Columns("Total").FillWeight = 30

                                'Seleccionar la fila por idcuenta guardado
                                For Each row As DataGridViewRow In dgvClientes.Rows
                                    If row.Cells("IdCuenta").Value = idColumnaSeleccionada Then
                                        dgvClientes.Rows(row.Index).Cells("Nombre").Selected = True
                                        Exit For
                                    End If
                                Next

                                'scroll a donde se encuentra la fila seleccionada
                                Dim halfway As Integer = dgvClientes.DisplayedRowCount(False) / 2
                                If dgvClientes.FirstDisplayedScrollingRowIndex + halfway > dgvClientes.SelectedRows(0).Index Or dgvClientes.FirstDisplayedScrollingRowIndex + dgvClientes.DisplayedRowCount(False) - halfway <= dgvClientes.SelectedRows(0).Index Then
                                    Dim targetRow As Integer = dgvClientes.SelectedRows(0).Index
                                    targetRow = Math.Max(targetRow - halfway, 0)
                                    dgvClientes.FirstDisplayedScrollingRowIndex = targetRow
                                End If

                                ' Colorear tablas
                                For Each row As DataGridViewRow In dgvClientes.Rows
                                    row.DefaultCellStyle.BackColor = Color.Gray
                                Next

                                For Each row As DataGridViewRow In dgvMovimientos.Rows
                                    If row.Cells("Tipo").Value = "Pago" Then
                                        row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
                                    ElseIf row.Cells("Tipo").Value = "Compra" Then
                                        row.DefaultCellStyle.BackColor = Color.Salmon
                                    End If
                                Next
#End Region

                                MsgBox("Pago registrado.")

                            Catch ex As Exception
                                MessageBox.Show("Ocurrió un problema al cargar la lista de clientes. Detalle:" & ex.Message.ToString)
                            End Try

                            If dgvClientes.RowCount > 0 Then
                                bttCancelarSeleccion.Enabled = True
                            Else
                                bttCancelarSeleccion.Enabled = False
                            End If

                        Catch ex As Exception

                        End Try

                    Else
                        MsgBox("El cliente no registra ninguna deuda.")
                    End If
                Else
                    MsgBox("El monto ingresado no puede ser mayor al saldo.")
                End If
            Else
                MsgBox("Ingrese un monto válido.")
            End If
        Catch ex As Exception

        End Try



    End Sub

    Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress
        'Verifica que solo se ingresen NUMEROS ENTEROS y la "coma"
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                If Asc(e.KeyChar) = 44 Then 'si quieres q sea "PUNTO" pone 46
                    Dim count As Integer = CountCharacter(txtMonto.Text, e.KeyChar)
                    If count <> 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If

            End If
        End If
    End Sub

    Private Sub bttCancelarSeleccion_Click(sender As Object, e As EventArgs) Handles bttCancelarSeleccion.Click

        bttCrearCuenta.Enabled = True
        dgvClientes.Enabled = True

        For Each row As DataGridViewRow In dgvClientes.Rows
            row.DefaultCellStyle.BackColor = Color.White
        Next

        lblCliente.Text = "Cliente:"
        dgvMovimientos.DataSource = Nothing
        lblTotal.Text = "Total adeudado: $ 0"

    End Sub

    Private Sub dgvMovimientos_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvMovimientos.CellMouseDoubleClick

        If dgvMovimientos.SelectedRows(0).Cells("Tipo").Value = "Compra" Then

            If dgvMovimientos.SelectedRows.Count <> 0 And dgvMovimientos.SelectedRows(0).Cells("IdVenta").Value <> 0 Then

                Dim f As New DetallesCuenta(dgvMovimientos.SelectedRows(0).Cells("IdVenta").Value)

                'pregunto si Ok(en la otra ventana declare que el boton cobrar es OK) fue presionado
                If f.ShowDialog() = DialogResult.OK Then

                Else

                End If
            End If
        End If

    End Sub

    Private Sub bttCrear_Click(sender As Object, e As EventArgs) Handles bttCrear.Click

        Try
            If Trim(txtMontoCredito.Text) <> "" And txtMontoCredito.Text <> 0 Then

                Try
                    Dim detalle As New DatosCuentasCorrientes
                    detalle.Fecha_ = dtpFechaCredito.Value
                    detalle.Monto_ = txtMontoCredito.Text
                    detalle.Tipo_ = "Compra"
                    detalle.IdCuenta_ = dgvClientes.SelectedRows(0).Cells("IdCuenta").Value
                    detalle.CrearDetalle()
                    detalle.IncrementarSaldo()

                    detalle.IdCliente_ = dgvClientes.SelectedRows(0).Cells("IdCliente").Value
                    dgvMovimientos.DataSource = detalle.ObtenerDetalles()
                    dgvMovimientos.Columns("IdDetalle").Visible = False
                    dgvMovimientos.Columns("IdCuenta").Visible = False
                    dgvMovimientos.Columns("Saldo").Visible = False
                    dgvMovimientos.Columns("Fecha").FillWeight = 40
                    dgvMovimientos.Columns("Monto").FillWeight = 30
                    dgvMovimientos.Columns("Tipo").FillWeight = 30

                    lblTotal.Text = "Total adeudado: $" & dgvMovimientos.SelectedRows(0).Cells("Saldo").Value
                    txtMontoCredito.Text = ""
                    dtpFechaCredito.Value = Date.Now


#Region "Actualizar tabla clientes y seleccionar la fila"
                    ' Actualizar tabla clientes
                    Dim cli As New DatosCuentasCorrientes
                    Dim dt As New DataTable
                    Try
                        dgvClientes.DataSource = cli.BuscarCuentas(dt, txtBuscar.Text)
                        dgvClientes.Columns("IdCliente").Visible = False
                        dgvClientes.Columns("IdCuenta").Visible = False
                        dgvClientes.Columns("Documento").FillWeight = 30
                        dgvClientes.Columns("Nombre").FillWeight = 40
                        dgvClientes.Columns("Total").FillWeight = 30

                        'Seleccionar la fila por idcuenta guardado
                        For Each row As DataGridViewRow In dgvClientes.Rows
                            If row.Cells("IdCuenta").Value = idColumnaSeleccionada Then
                                dgvClientes.Rows(row.Index).Cells("Nombre").Selected = True
                                Exit For
                            End If
                        Next

                        'scroll a donde se encuentra la fila seleccionada
                        Dim halfway As Integer = dgvClientes.DisplayedRowCount(False) / 2
                        If dgvClientes.FirstDisplayedScrollingRowIndex + halfway > dgvClientes.SelectedRows(0).Index Or dgvClientes.FirstDisplayedScrollingRowIndex + dgvClientes.DisplayedRowCount(False) - halfway <= dgvClientes.SelectedRows(0).Index Then
                            Dim targetRow As Integer = dgvClientes.SelectedRows(0).Index
                            targetRow = Math.Max(targetRow - halfway, 0)
                            dgvClientes.FirstDisplayedScrollingRowIndex = targetRow
                        End If

                        ' Colorear tablas
                        For Each row As DataGridViewRow In dgvClientes.Rows
                            row.DefaultCellStyle.BackColor = Color.Gray
                        Next

                        For Each row As DataGridViewRow In dgvMovimientos.Rows
                            If row.Cells("Tipo").Value = "Pago" Then
                                row.DefaultCellStyle.BackColor = Color.MediumSpringGreen
                            ElseIf row.Cells("Tipo").Value = "Compra" Then
                                row.DefaultCellStyle.BackColor = Color.Salmon
                            End If
                        Next
#End Region

                        MsgBox("Crédito registrado.")

                    Catch ex As Exception
                        MessageBox.Show("Ocurrió un problema al cargar la lista de clientes. Detalle:" & ex.Message.ToString)
                    End Try

                    If dgvClientes.RowCount > 0 Then
                        bttCancelarSeleccion.Enabled = True
                    Else
                        bttCancelarSeleccion.Enabled = False
                    End If

                Catch ex As Exception

                End Try
            Else
                MsgBox("Ingrese un monto válido.")
            End If
        Catch ex As Exception

        End Try


    End Sub

    Private Sub bttCrearCuenta_Click(sender As Object, e As EventArgs) Handles bttCrearCuenta.Click


        Dim f As New ElegirCliente
        f.b = 0

        Try
            If f.ShowDialog() = DialogResult.OK Then

                Dim verificar As New DatosCuentasCorrientes
                Dim dtCuentaCorriente As New DataTable
                verificar.IdCliente_ = f.DataGridView1.SelectedCells.Item(0).Value.ToString
                dtCuentaCorriente = verificar.VerificarCuenta

                If dtCuentaCorriente.Rows.Count > 0 Then
                    ' Existe
                    MessageBox.Show("Ya existe una cuenta corriente creada para el cliente seleccionado. Verifique en la lista.")
                Else
                    ' No existe
                    Dim cuenta As New DatosCuentasCorrientes
                    ' Controlar que haya un id cargado
                    cuenta.IdCliente_ = f.DataGridView1.SelectedCells.Item(0).Value.ToString
                    cuenta.Saldo_ = 0
                    Dim idCuenta As Long
                    idCuenta = cuenta.CrearCuenta()

                    MessageBox.Show("Cuenta corriente creada exitosamente.")

                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub
End Class