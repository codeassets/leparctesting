﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Productos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.bttAgregarProd = New System.Windows.Forms.Button()
        Me.bttEditarProd = New System.Windows.Forms.Button()
        Me.bttEliminar = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TLP_Datos = New System.Windows.Forms.TableLayoutPanel()
        Me.txtPrecioMi = New System.Windows.Forms.TextBox()
        Me.txtDesc = New System.Windows.Forms.TextBox()
        Me.txtPrecioTa = New System.Windows.Forms.TextBox()
        Me.txtPrecioMa = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCod = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NumCant = New System.Windows.Forms.NumericUpDown()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.tb_colorname = New System.Windows.Forms.TextBox()
        Me.bttColor = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.CBoxMarca = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CBoxTalle = New System.Windows.Forms.ComboBox()
        Me.CB_Buscar = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.bttCancelar = New System.Windows.Forms.Button()
        Me.bttGuardar = New System.Windows.Forms.Button()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.bttFaltantes = New System.Windows.Forms.Button()
        Me.dgvStock = New System.Windows.Forms.DataGridView()
        Me.TLP_Datos.SuspendLayout()
        CType(Me.NumCant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.dgvStock, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bttAgregarProd
        '
        Me.bttAgregarProd.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.bttAgregarProd.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.bttAgregarProd.FlatAppearance.BorderSize = 0
        Me.bttAgregarProd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttAgregarProd.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttAgregarProd.ForeColor = System.Drawing.Color.Black
        Me.bttAgregarProd.Image = Global.LeParc.My.Resources.Resources.mas__1_
        Me.bttAgregarProd.Location = New System.Drawing.Point(22, 3)
        Me.bttAgregarProd.Name = "bttAgregarProd"
        Me.bttAgregarProd.Size = New System.Drawing.Size(88, 44)
        Me.bttAgregarProd.TabIndex = 0
        Me.bttAgregarProd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.bttAgregarProd, "Agregar")
        Me.bttAgregarProd.UseVisualStyleBackColor = True
        '
        'bttEditarProd
        '
        Me.bttEditarProd.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.bttEditarProd.Enabled = False
        Me.bttEditarProd.FlatAppearance.BorderSize = 0
        Me.bttEditarProd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttEditarProd.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttEditarProd.ForeColor = System.Drawing.Color.Black
        Me.bttEditarProd.Image = Global.LeParc.My.Resources.Resources.lapiz
        Me.bttEditarProd.Location = New System.Drawing.Point(155, 3)
        Me.bttEditarProd.Name = "bttEditarProd"
        Me.bttEditarProd.Size = New System.Drawing.Size(88, 44)
        Me.bttEditarProd.TabIndex = 1
        Me.bttEditarProd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.bttEditarProd, "Editar")
        Me.bttEditarProd.UseVisualStyleBackColor = True
        '
        'bttEliminar
        '
        Me.bttEliminar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.bttEliminar.Enabled = False
        Me.bttEliminar.FlatAppearance.BorderSize = 0
        Me.bttEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttEliminar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttEliminar.ForeColor = System.Drawing.Color.Black
        Me.bttEliminar.Image = Global.LeParc.My.Resources.Resources.menos
        Me.bttEliminar.Location = New System.Drawing.Point(287, 3)
        Me.bttEliminar.Name = "bttEliminar"
        Me.bttEliminar.Size = New System.Drawing.Size(90, 44)
        Me.bttEliminar.TabIndex = 3
        Me.bttEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.bttEliminar, "Eliminar")
        Me.bttEliminar.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Image = Global.LeParc.My.Resources.Resources.add
        Me.Button1.Location = New System.Drawing.Point(212, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(34, 27)
        Me.Button1.TabIndex = 1
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.Button1, "Agregar marca")
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TLP_Datos
        '
        Me.TLP_Datos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TLP_Datos.ColumnCount = 2
        Me.TLP_Datos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TLP_Datos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TLP_Datos.Controls.Add(Me.txtPrecioMi, 1, 6)
        Me.TLP_Datos.Controls.Add(Me.txtDesc, 1, 1)
        Me.TLP_Datos.Controls.Add(Me.txtPrecioTa, 1, 8)
        Me.TLP_Datos.Controls.Add(Me.txtPrecioMa, 1, 7)
        Me.TLP_Datos.Controls.Add(Me.Label3, 0, 0)
        Me.TLP_Datos.Controls.Add(Me.Label4, 0, 1)
        Me.TLP_Datos.Controls.Add(Me.txtCod, 1, 0)
        Me.TLP_Datos.Controls.Add(Me.Label5, 0, 2)
        Me.TLP_Datos.Controls.Add(Me.Label7, 0, 4)
        Me.TLP_Datos.Controls.Add(Me.Label8, 0, 5)
        Me.TLP_Datos.Controls.Add(Me.NumCant, 1, 5)
        Me.TLP_Datos.Controls.Add(Me.TableLayoutPanel1, 1, 4)
        Me.TLP_Datos.Controls.Add(Me.Label6, 0, 3)
        Me.TLP_Datos.Controls.Add(Me.TableLayoutPanel3, 1, 2)
        Me.TLP_Datos.Controls.Add(Me.Label9, 0, 6)
        Me.TLP_Datos.Controls.Add(Me.Label1, 0, 8)
        Me.TLP_Datos.Controls.Add(Me.Label2, 0, 7)
        Me.TLP_Datos.Controls.Add(Me.CBoxTalle, 1, 3)
        Me.TLP_Datos.Enabled = False
        Me.TLP_Datos.Location = New System.Drawing.Point(600, 170)
        Me.TLP_Datos.Margin = New System.Windows.Forms.Padding(20, 3, 20, 3)
        Me.TLP_Datos.Name = "TLP_Datos"
        Me.TLP_Datos.RowCount = 9
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TLP_Datos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLP_Datos.Size = New System.Drawing.Size(399, 302)
        Me.TLP_Datos.TabIndex = 32
        '
        'txtPrecioMi
        '
        Me.txtPrecioMi.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPrecioMi.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtPrecioMi.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPrecioMi.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioMi.Location = New System.Drawing.Point(153, 204)
        Me.txtPrecioMi.MaxLength = 11
        Me.txtPrecioMi.Name = "txtPrecioMi"
        Me.txtPrecioMi.Size = New System.Drawing.Size(243, 20)
        Me.txtPrecioMi.TabIndex = 6
        '
        'txtDesc
        '
        Me.txtDesc.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDesc.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtDesc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDesc.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDesc.Location = New System.Drawing.Point(153, 39)
        Me.txtDesc.MaxLength = 50
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(243, 20)
        Me.txtDesc.TabIndex = 1
        '
        'txtPrecioTa
        '
        Me.txtPrecioTa.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPrecioTa.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtPrecioTa.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPrecioTa.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioTa.Location = New System.Drawing.Point(153, 273)
        Me.txtPrecioTa.MaxLength = 11
        Me.txtPrecioTa.Name = "txtPrecioTa"
        Me.txtPrecioTa.Size = New System.Drawing.Size(243, 20)
        Me.txtPrecioTa.TabIndex = 8
        '
        'txtPrecioMa
        '
        Me.txtPrecioMa.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPrecioMa.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtPrecioMa.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPrecioMa.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioMa.Location = New System.Drawing.Point(153, 237)
        Me.txtPrecioMa.MaxLength = 11
        Me.txtPrecioMa.Name = "txtPrecioMa"
        Me.txtPrecioMa.Size = New System.Drawing.Size(243, 20)
        Me.txtPrecioMa.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 20)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "Código"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 20)
        Me.Label4.TabIndex = 33
        Me.Label4.Text = "Descripción"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCod
        '
        Me.txtCod.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCod.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtCod.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCod.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCod.Location = New System.Drawing.Point(153, 6)
        Me.txtCod.MaxLength = 50
        Me.txtCod.Name = "txtCod"
        Me.txtCod.Size = New System.Drawing.Size(243, 20)
        Me.txtCod.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 20)
        Me.Label5.TabIndex = 34
        Me.Label5.Text = "Marca"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 138)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 20)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "Color"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(3, 171)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 20)
        Me.Label8.TabIndex = 37
        Me.Label8.Text = "Cantidad"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'NumCant
        '
        Me.NumCant.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NumCant.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.NumCant.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NumCant.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumCant.Location = New System.Drawing.Point(153, 170)
        Me.NumCant.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.NumCant.Name = "NumCant"
        Me.NumCant.Size = New System.Drawing.Size(243, 23)
        Me.NumCant.TabIndex = 5
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.tb_colorname, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.bttColor, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(150, 132)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(249, 33)
        Me.TableLayoutPanel1.TabIndex = 4
        '
        'tb_colorname
        '
        Me.tb_colorname.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tb_colorname.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.tb_colorname.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_colorname.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_colorname.Location = New System.Drawing.Point(127, 6)
        Me.tb_colorname.Name = "tb_colorname"
        Me.tb_colorname.Size = New System.Drawing.Size(119, 20)
        Me.tb_colorname.TabIndex = 1
        '
        'bttColor
        '
        Me.bttColor.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttColor.BackColor = System.Drawing.Color.White
        Me.bttColor.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.bttColor.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(207, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.bttColor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(207, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.bttColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttColor.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttColor.ForeColor = System.Drawing.Color.Black
        Me.bttColor.Location = New System.Drawing.Point(3, 3)
        Me.bttColor.Name = "bttColor"
        Me.bttColor.Size = New System.Drawing.Size(118, 27)
        Me.bttColor.TabIndex = 0
        Me.bttColor.Text = "Elegir"
        Me.bttColor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttColor.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 20)
        Me.Label6.TabIndex = 35
        Me.Label6.Text = "Talle"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Button1, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CBoxMarca, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(150, 66)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(249, 33)
        Me.TableLayoutPanel3.TabIndex = 2
        '
        'CBoxMarca
        '
        Me.CBoxMarca.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CBoxMarca.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.CBoxMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBoxMarca.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBoxMarca.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBoxMarca.FormattingEnabled = True
        Me.CBoxMarca.Location = New System.Drawing.Point(3, 3)
        Me.CBoxMarca.Name = "CBoxMarca"
        Me.CBoxMarca.Size = New System.Drawing.Size(203, 28)
        Me.CBoxMarca.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(3, 204)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(130, 20)
        Me.Label9.TabIndex = 38
        Me.Label9.Text = "Precio Minorista"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 273)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(144, 20)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Precio con Tarjeta"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 237)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(135, 20)
        Me.Label2.TabIndex = 45
        Me.Label2.Text = "Precio Mayorista"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CBoxTalle
        '
        Me.CBoxTalle.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CBoxTalle.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.CBoxTalle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBoxTalle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBoxTalle.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBoxTalle.FormattingEnabled = True
        Me.CBoxTalle.Location = New System.Drawing.Point(153, 102)
        Me.CBoxTalle.Name = "CBoxTalle"
        Me.CBoxTalle.Size = New System.Drawing.Size(243, 28)
        Me.CBoxTalle.TabIndex = 3
        '
        'CB_Buscar
        '
        Me.CB_Buscar.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.CB_Buscar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CB_Buscar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CB_Buscar.FormattingEnabled = True
        Me.CB_Buscar.Items.AddRange(New Object() {"Código", "Talle", "Marca", "Color", "Descripción", "Descripción y Marca"})
        Me.CB_Buscar.Location = New System.Drawing.Point(322, 75)
        Me.CB_Buscar.Name = "CB_Buscar"
        Me.CB_Buscar.Size = New System.Drawing.Size(195, 28)
        Me.CB_Buscar.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(25, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(158, 38)
        Me.Label10.TabIndex = 36
        Me.Label10.Text = "Productos"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBuscar
        '
        Me.txtBuscar.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtBuscar.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBuscar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscar.ForeColor = System.Drawing.Color.Black
        Me.txtBuscar.Location = New System.Drawing.Point(32, 78)
        Me.txtBuscar.Margin = New System.Windows.Forms.Padding(0)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(273, 20)
        Me.txtBuscar.TabIndex = 38
        Me.txtBuscar.Text = "Presione ENTER para buscar"
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(600, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(288, 38)
        Me.Label11.TabIndex = 39
        Me.Label11.Text = "Datos del producto"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bttCancelar
        '
        Me.bttCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttCancelar.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.bttCancelar.Enabled = False
        Me.bttCancelar.FlatAppearance.BorderSize = 0
        Me.bttCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttCancelar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttCancelar.ForeColor = System.Drawing.Color.Black
        Me.bttCancelar.Location = New System.Drawing.Point(748, 489)
        Me.bttCancelar.Margin = New System.Windows.Forms.Padding(0)
        Me.bttCancelar.Name = "bttCancelar"
        Me.bttCancelar.Size = New System.Drawing.Size(121, 40)
        Me.bttCancelar.TabIndex = 75
        Me.bttCancelar.Text = "Cancelar"
        Me.bttCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttCancelar.UseVisualStyleBackColor = False
        '
        'bttGuardar
        '
        Me.bttGuardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(206, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.bttGuardar.Enabled = False
        Me.bttGuardar.FlatAppearance.BorderSize = 0
        Me.bttGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttGuardar.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttGuardar.ForeColor = System.Drawing.Color.Black
        Me.bttGuardar.Location = New System.Drawing.Point(875, 489)
        Me.bttGuardar.Margin = New System.Windows.Forms.Padding(0)
        Me.bttGuardar.Name = "bttGuardar"
        Me.bttGuardar.Size = New System.Drawing.Size(121, 40)
        Me.bttGuardar.TabIndex = 74
        Me.bttGuardar.Text = "Guardar"
        Me.bttGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttGuardar.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.Controls.Add(Me.bttAgregarProd, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.bttEditarProd, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.bttEliminar, 2, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(600, 114)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(399, 50)
        Me.TableLayoutPanel2.TabIndex = 76
        '
        'bttFaltantes
        '
        Me.bttFaltantes.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttFaltantes.BackColor = System.Drawing.Color.FromArgb(CType(CType(206, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.bttFaltantes.FlatAppearance.BorderSize = 0
        Me.bttFaltantes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(207, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.bttFaltantes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(207, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.bttFaltantes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttFaltantes.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttFaltantes.ForeColor = System.Drawing.Color.Black
        Me.bttFaltantes.Location = New System.Drawing.Point(442, 489)
        Me.bttFaltantes.Name = "bttFaltantes"
        Me.bttFaltantes.Size = New System.Drawing.Size(126, 40)
        Me.bttFaltantes.TabIndex = 77
        Me.bttFaltantes.Text = "Ver faltantes"
        Me.bttFaltantes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttFaltantes.UseVisualStyleBackColor = False
        '
        'dgvStock
        '
        Me.dgvStock.AllowUserToAddRows = False
        Me.dgvStock.AllowUserToDeleteRows = False
        Me.dgvStock.AllowUserToResizeRows = False
        Me.dgvStock.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvStock.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvStock.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvStock.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvStock.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvStock.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvStock.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvStock.ColumnHeadersHeight = 30
        Me.dgvStock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(206, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(87, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvStock.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvStock.Location = New System.Drawing.Point(32, 113)
        Me.dgvStock.MultiSelect = False
        Me.dgvStock.Name = "dgvStock"
        Me.dgvStock.ReadOnly = True
        Me.dgvStock.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvStock.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvStock.RowHeadersVisible = False
        Me.dgvStock.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStock.ShowEditingIcon = False
        Me.dgvStock.Size = New System.Drawing.Size(536, 359)
        Me.dgvStock.TabIndex = 78
        '
        'Productos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1037, 569)
        Me.Controls.Add(Me.dgvStock)
        Me.Controls.Add(Me.bttFaltantes)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.bttCancelar)
        Me.Controls.Add(Me.bttGuardar)
        Me.Controls.Add(Me.TLP_Datos)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.CB_Buscar)
        Me.Controls.Add(Me.txtBuscar)
        Me.Controls.Add(Me.Label10)
        Me.KeyPreview = True
        Me.Name = "Productos"
        Me.Text = "Productos"
        Me.TLP_Datos.ResumeLayout(False)
        Me.TLP_Datos.PerformLayout()
        CType(Me.NumCant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.dgvStock, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents bttEditarProd As Button
    Friend WithEvents bttAgregarProd As Button
    Friend WithEvents TLP_Datos As TableLayoutPanel
    Friend WithEvents txtDesc As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtCod As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents NumCant As NumericUpDown
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents tb_colorname As TextBox
    Friend WithEvents bttColor As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents Button1 As Button
    Friend WithEvents txtPrecioMi As TextBox
    Friend WithEvents txtPrecioMa As TextBox
    Friend WithEvents txtPrecioTa As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents CBoxMarca As ComboBox
    Friend WithEvents CBoxTalle As ComboBox
    Friend WithEvents CB_Buscar As ComboBox
    Friend WithEvents bttEliminar As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents txtBuscar As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents bttCancelar As Button
    Friend WithEvents bttGuardar As Button
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents bttFaltantes As Button
    Friend WithEvents dgvStock As DataGridView
End Class
