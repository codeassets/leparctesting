﻿Public Class NuevaVenta

    Dim datosventas As New DatosVentas
    Dim datosind As New DatosProductos

    Dim idprod As Integer
    Public idprod_buscar As Integer = 0

    Dim totalmin As Double
    Dim totalmay As Double
    Dim totaltarj As Double
    Dim totalconDesc As Double
    Dim total As Double

    Dim cantidad As Integer
    Dim descuento As Double
    Dim Stock As Integer

    Public idcliente As Integer
    Public documentocliente As Int64
    Public nombrecliente As String
    Public direccioncliente As String
    Public tipopago As String

#Region "Funciones"

    Public Sub Cargar_Datos()

        Dim p As New DatosProductos
        Dim dt As New DataTable

        If (idprod_buscar = 0) Then
            p.Codigo_ = txtCodVenta.Text
            dt = p.BuscarxCodigo()
        Else
            p.IdProducto_ = idprod_buscar
            p.Buscar(dt, 6)
        End If

        If dt.Rows.Count <> 0 Then

            txtCodVenta.Text = dt.Rows(0)("Codigo")
            txtMarca.Text = dt.Rows(0)("Marca")
            txtDescVenta.Text = dt.Rows(0)("Descripcion")
            txtTalle.Text = dt.Rows(0)("Talle")
            txtColorVenta.Text = dt.Rows(0)("NombreColor")
            txtPrecioVenta.Text = dt.Rows(0)("PrecioMinorista")
            txtPrecioMay.Text = dt.Rows(0)("PrecioMayorista")
            txtPrecioTarj.Text = dt.Rows(0)("PrecioTarjeta")

            NumCantVenta.Maximum = dt.Rows(0)("Cantidad")
            idprod = dt.Rows(0)("IdProducto")
            idprod_buscar = dt.Rows(0)("IdProducto")
            Stock = dt.Rows(0)("Cantidad")

            'el maximo es la diferencia entre el valor del datagridview y 
            ' el valor q se acaba de encontrar

            Dim b As Integer = 0

            For Each row As DataGridViewRow In dgvAgregados.Rows

                If idprod = row.Cells("Id").Value Then
                    NumCantVenta.Maximum = Stock - row.Cells("Cantidad").Value
                    b = 1
                    Exit For
                End If

            Next

            If b = 0 Then
                NumCantVenta.Maximum = Stock
            End If

            If NumCantVenta.Maximum = 0 Then
                bttAgregar.Enabled = False
                'CB_Precio.Enabled = False
                NumCantVenta.Enabled = False
                LabelNoStock.Visible = True
            Else
                bttAgregar.Enabled = True
                'CB_Precio.Enabled = True
                NumCantVenta.Enabled = True
                LabelNoStock.Visible = False
            End If

            idprod = dt.Rows(0)("IdProducto")

            txtDescVenta.Text = dt.Rows(0)("Descripcion")

            txtTalle.Text = dt.Rows(0)("Talle")
            txtColorVenta.Text = dt.Rows(0)("NombreColor")
            txtPrecioVenta.Text = dt.Rows(0)("PrecioMinorista")
            txtPrecioMay.Text = dt.Rows(0)("PrecioMayorista")
            txtPrecioTarj.Text = dt.Rows(0)("PrecioTarjeta")

            NumCantVenta.Select(0, NumCantVenta.Text.Length)
            txtCodVenta.Enabled = False
            NumCantVenta.Focus()

        End If

        bttModificarProducto.Enabled = True

    End Sub

#End Region

    Private Sub bttElegirCliente_Click(sender As Object, e As EventArgs) Handles bttElegirCliente.Click

        Dim f As New ElegirCliente
        f.b = 0

        Try
            If f.ShowDialog() = DialogResult.OK Then
                labelCliente.Text = f.DataGridView1.SelectedCells.Item(3).Value.ToString + ", " + f.DataGridView1.SelectedCells.Item(2).Value.ToString

                idcliente = f.DataGridView1.SelectedCells.Item(0).Value.ToString
                documentocliente = f.DataGridView1.SelectedCells.Item(1).Value.ToString
                nombrecliente = f.DataGridView1.SelectedCells.Item(2).Value.ToString & " " & f.DataGridView1.SelectedCells.Item(3).Value.ToString
                direccioncliente = f.DataGridView1.SelectedCells.Item(4).Value.ToString

                txtCodVenta.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        labelCliente.Text = "Seleccione un cliente"
        idcliente = Nothing
        documentocliente = 0
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ' Al elegir un producto con sock 0 la cantidad a añadir se pone en 0,
        ' y al buscar un nuevo producto, sigue estando en 0.
        ' Reiniciar la cantidad de productos a agregar al seleccionar nuevo producto.
        NumCantVenta.Maximum = 1
        NumCantVenta.Value = 1

        Dim f As New BuscarProducto

        Try
            If f.ShowDialog() = DialogResult.OK Then
                idprod_buscar = f.dgvStock.CurrentRow.Cells("IdProducto").Value
                Cargar_Datos()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            'FocusMe()
            'NumCantVenta.Select(0, NumCantVenta.Text.Length)
            'NumCantVenta.Select()
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        idprod_buscar = 0
        idprod = 0
        txtCodVenta.Enabled = True
        txtDescVenta.Clear()
        txtTalle.Clear()
        'CB_Precio.SelectedIndex = -1
        NumCantVenta.Maximum = 1000
        NumCantVenta.Value = 1
        txtColorVenta.Clear()
        txtPrecioVenta.Text = "Min."
        txtPrecioTarj.Text = "Tarj."
        txtPrecioMay.Text = "May."
        'CB_Precio.Enabled = False
        NumCantVenta.Enabled = False
        bttAgregar.Enabled = False
        LabelNoStock.Visible = False
        txtCodVenta.Clear()
        txtCodVenta.Focus()
        bttModificarProducto.Enabled = False
    End Sub

    Private Sub txtCodVenta_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles txtCodVenta.PreviewKeyDown
        If e.KeyCode = Keys.Enter Then
            If idprod_buscar = 0 Then
                Cargar_Datos()
            End If
        End If
    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click
        dgvAgregados.ClearSelection()

        Dim tipo As String
        Dim pago As String

        If cboxPrecio.SelectedIndex = 0 Then 'minorista y efectivo
            pago = "Efectivo"
            tipo = "Minorista"
        ElseIf cboxPrecio.SelectedIndex = 1 Then 'minorista y tarjeta
            pago = "Tarjeta"
            tipo = "Minorista"
        ElseIf cboxPrecio.SelectedIndex = 2 Then  'minorista y debito
            pago = "Débito"
            tipo = "Minorista"
        ElseIf cboxPrecio.SelectedIndex = 3 Then  'mayorista y efectivo
            pago = "Efectivo"
            tipo = "Mayorista"
        ElseIf cboxPrecio.SelectedIndex = 4 Then  'cuenta corriente minorista
            pago = "Cuenta Corriente"
            tipo = "Minorista"
        ElseIf cboxPrecio.SelectedIndex = 5 Then  'cuenta corriente mayorista
            pago = "Cuenta Corriente"
            tipo = "Mayorista"
        ElseIf cboxPrecio.SelectedIndex = 6 Then  'mercadopago
            pago = "Mercadopago"
            tipo = "Minorista"
        End If

        If txtCodVenta.Text <> "" And NumCantVenta.Value <> 0 Then

            Dim b As Integer = 0
            Dim p As New DatosProductos

            p.Codigo_ = txtCodVenta.Text

            If dgvAgregados.Rows.Count <> 0 Then

                For Each row As DataGridViewRow In dgvAgregados.Rows

                    If idprod = row.Cells("Id").Value Then 'And row.Cells("Tipo").Value = tipo And row.Cells("Pago").Value = pago Then

                        'If Not row.Cells("Tipo").Value = tipo And row.Cells("Pago").Value = pago Then

                        cantidad = cantidad + NumCantVenta.Value

                        ' End If

                        row.Cells("Cantidad").Value = row.Cells("Cantidad").Value + NumCantVenta.Value

                        totalmin = totalmin + txtPrecioVenta.Text * NumCantVenta.Value
                        totalmay = totalmay + txtPrecioMay.Text * NumCantVenta.Value
                        totaltarj = totaltarj + txtPrecioTarj.Text * NumCantVenta.Value

                        NumCantVenta.Maximum = Stock - row.Cells("Cantidad").Value
                        b = 1

                        Exit For
                    End If
                Next
            End If

            If b = 0 Then ' Producto no encontrado en la tabla agregados
                dgvAgregados.Rows.Add(New String() {idprod, txtCodVenta.Text, txtDescVenta.Text & " - " & txtMarca.Text & " - " & txtTalle.Text & " - " & txtColorVenta.Text,
                                      txtMarca.Text, txtDescVenta.Text, txtTalle.Text, txtColorVenta.Text, NumCantVenta.Value, txtPrecioVenta.Text, txtPrecioMay.Text, txtPrecioTarj.Text, tipo, pago})

                cantidad = cantidad + NumCantVenta.Value
                totalmin = totalmin + txtPrecioVenta.Text * NumCantVenta.Value
                totalmay = totalmay + txtPrecioMay.Text * NumCantVenta.Value
                totaltarj = totaltarj + txtPrecioTarj.Text * NumCantVenta.Value
                NumCantVenta.Maximum = Stock - NumCantVenta.Value

            End If

            idprod_buscar = 0
            txtCodVenta.Clear()
            txtDescVenta.Clear()
            txtMarca.Clear()
            txtTalle.Clear()
            NumCantVenta.Maximum = 1000
            NumCantVenta.Value = 1
            txtColorVenta.Clear()
            txtPrecioVenta.Text = "Min."
            txtPrecioTarj.Text = "Tarj."
            txtPrecioMay.Text = "May."
            cboxPrecio.SelectedIndex = 0

        End If

        If txtDescuento.Text = 0 Then ' Si no hay descuento

            If cboxPrecio.SelectedIndex = 0 Then 'minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta
                total = totaltarj
            ElseIf cboxPrecio.SelectedIndex = 2 Then 'debito
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 3 Then 'mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 4 Then 'cuenta corriente minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 5 Then 'cuenta corriente mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 6 Then 'mercadopago
                total = totalmin
            End If

            descuento = 0
            totalconDesc = total
            LabelTotal.Text = "Total: $ " & totalconDesc
            labelDescuento.Text = "Descuento: $ " & descuento
            txtTotal.Text = totalconDesc

        Else ' Si hay descuento

            If cboxPrecio.SelectedIndex = 0 Then 'minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta
                total = totaltarj
            ElseIf cboxPrecio.SelectedIndex = 2 Then 'debito
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 3 Then 'mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 4 Then 'cuenta corriente minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 5 Then 'cuenta corriente mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 6 Then 'mercadopago
                total = totalmin
            End If

            descuento = (total * txtDescuento.Text) / 100
            totalconDesc = total - (txtDescuento.Text * total) / 100
            LabelTotal.Text = "Total: $ " & totalconDesc
            labelDescuento.Text = "Descuento: $ " & descuento
            txtTotal.Text = totalconDesc

        End If

        NumCantVenta.Enabled = False
        bttAgregar.Enabled = False
        txtCodVenta.Enabled = True
        txtCodVenta.Focus()

    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        totalmin = totalmin - Double.Parse(dgvAgregados.CurrentRow.Cells.Item("Precio Min.").Value) * Integer.Parse(dgvAgregados.CurrentRow.Cells.Item("Cantidad").Value)
        totalmay = totalmay - Double.Parse(dgvAgregados.CurrentRow.Cells.Item("Precio May.").Value) * Integer.Parse(dgvAgregados.CurrentRow.Cells.Item("Cantidad").Value)
        totaltarj = totaltarj - Double.Parse(dgvAgregados.CurrentRow.Cells.Item("Precio Tarj.").Value) * Integer.Parse(dgvAgregados.CurrentRow.Cells.Item("Cantidad").Value)

        If txtDescuento.Text = 0 Then ' Si no hay descuento

            If cboxPrecio.SelectedIndex = 0 Then 'minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta
                total = totaltarj
            ElseIf cboxPrecio.SelectedIndex = 2 Then 'debito
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 3 Then 'mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 4 Then 'cuenta corriente minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 5 Then 'cuenta corriente mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 6 Then 'mercadopago
                total = totalmin
            End If

            descuento = 0
            totalconDesc = total
            LabelTotal.Text = "Total: $ " & totalconDesc
            labelDescuento.Text = "Descuento: $ " & descuento
            txtTotal.Text = totalconDesc

        Else ' Si hay descuento

            If cboxPrecio.SelectedIndex = 0 Then 'minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta
                total = totaltarj
            ElseIf cboxPrecio.SelectedIndex = 2 Then 'debito
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 3 Then 'mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 4 Then 'cuenta corriente minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 5 Then 'cuenta corriente mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 6 Then 'mercadopago
                total = totalmin
            End If

            descuento = (total * txtDescuento.Text) / 100
            totalconDesc = total - (txtDescuento.Text * total) / 100
            LabelTotal.Text = "Total: $ " & totalconDesc
            labelDescuento.Text = "Descuento: $ " & descuento
            txtTotal.Text = totalconDesc

        End If

        cantidad = cantidad - Integer.Parse(dgvAgregados.CurrentRow.Cells.Item("Cantidad").Value)
        NumCantVenta.Maximum = NumCantVenta.Maximum + dgvAgregados.CurrentRow.Cells.Item("Cantidad").Value
        dgvAgregados.Rows.RemoveAt(dgvAgregados.CurrentRow.Index)

    End Sub

    Private Sub bttBorrarProd_SelectionChanged(sender As Object, e As EventArgs) Handles dgvAgregados.SelectionChanged

        If dgvAgregados.SelectedRows.Count = 0 Then
            bttEliminar.Enabled = False
        Else
            bttEliminar.Enabled = True
        End If

        If dgvAgregados.Rows.Count = 0 Then
            bttFinalizar.Enabled = False
        Else
            bttFinalizar.Enabled = True
        End If

    End Sub

    Private Sub txtDescuento_Click(sender As Object, e As EventArgs) Handles txtDescuento.Click
        txtDescuento.SelectAll()
    End Sub



    Private Sub txtDescuento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescuento.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub cboxPrecio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxPrecio.SelectedIndexChanged

        If txtDescuento.Text = 0 Then ' Si no hay descuento

            If cboxPrecio.SelectedIndex = 0 Then 'minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta
                total = totaltarj
            ElseIf cboxPrecio.SelectedIndex = 2 Then 'debito
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 3 Then 'mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 4 Then 'cuenta corriente minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 5 Then 'cuenta corriente mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 6 Then 'mercadopago
                total = totalmin
            End If

            descuento = 0
            totalconDesc = total
            LabelTotal.Text = "Total: $ " & totalconDesc
            labelDescuento.Text = "Descuento: $ " & descuento
            txtTotal.Text = totalconDesc

        Else ' Si hay descuento

            If cboxPrecio.SelectedIndex = 0 Then 'minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta
                total = totaltarj
            ElseIf cboxPrecio.SelectedIndex = 2 Then 'debito
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 3 Then 'mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 4 Then 'cuenta corriente minorista
                total = totalmin
            ElseIf cboxPrecio.SelectedIndex = 5 Then 'cuenta corriente mayorista
                total = totalmay
            ElseIf cboxPrecio.SelectedIndex = 6 Then 'mercadopago
                total = totalmin
            End If

            descuento = (total * txtDescuento.Text) / 100
            totalconDesc = total - (txtDescuento.Text * total) / 100
            LabelTotal.Text = "Total: $ " & totalconDesc
            labelDescuento.Text = "Descuento: $ " & descuento
            txtTotal.Text = totalconDesc

        End If

    End Sub

    Private Sub bttFinalizar_Click(sender As Object, e As EventArgs) Handles bttFinalizar.Click
        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea finalizar la venta?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try

                ' Si la venta supera los 10000, envía un documento, si no, 99999999
                Dim param_documentocliente As Long = 0

                If totalconDesc < 10000 Then
                    If documentocliente = 0 Then
                        param_documentocliente = 99999999
                    Else
                        param_documentocliente = documentocliente
                    End If
                Else
                    If documentocliente = 0 Then
                        MsgBox("Seleccione un cliente")
                        Return
                    Else
                        param_documentocliente = documentocliente
                    End If
                End If

                If ((cboxPrecio.SelectedIndex = 4 Or cboxPrecio.SelectedIndex = 5) AndAlso idcliente <> Nothing) Or (cboxPrecio.SelectedIndex <> 4 And cboxPrecio.SelectedIndex <> 5) Then
                    ' Hay un cliente seleccionado para cuenta corriente o la venta no es por cc

#Region "Crear Venta"
                    datosventas.IdCliente_ = idcliente
                    datosventas.Fecha_ = Date.Today

                    'tipo y pago
                    If cboxPrecio.SelectedIndex = 0 Then 'minorista y efectivo
                        datosventas.Pago_ = "Efectivo"
                        datosventas.Tipo_ = "Minorista"
                        tipopago = "Efectivo"
                    ElseIf cboxPrecio.SelectedIndex = 1 Then 'minorista y tarjeta
                        datosventas.Pago_ = "Tarjeta"
                        datosventas.Tipo_ = "Minorista"
                        tipopago = "Tarjeta"
                    ElseIf cboxPrecio.SelectedIndex = 2 Then 'minorista y debito
                        datosventas.Pago_ = "Débito"
                        datosventas.Tipo_ = "Minorista"
                        tipopago = "Débito"
                    ElseIf cboxPrecio.SelectedIndex = 3 Then 'mayorista y efectivo
                        datosventas.Pago_ = "Efectivo"
                        datosventas.Tipo_ = "Mayorista"
                        tipopago = "Efectivo"
                    ElseIf cboxPrecio.SelectedIndex = 4 Then 'minorista y cuenta corriente
                        datosventas.Pago_ = "Cuenta Corriente"
                        datosventas.Tipo_ = "Minorista"
                        tipopago = "Efectivo"
                    ElseIf cboxPrecio.SelectedIndex = 5 Then 'mayorista y cuenta corriente
                        datosventas.Pago_ = "Cuenta Corriente"
                        datosventas.Tipo_ = "Mayorista"
                        tipopago = "Efectivo"
                    ElseIf cboxPrecio.SelectedIndex = 6 Then 'mercadopago
                        datosventas.Pago_ = "Mercadopago"
                        datosventas.Tipo_ = "Minorista"
                        tipopago = "Tarjeta"
                    End If

                    'total
                    'Dim t As Double
                    'For Each row As DataGridViewRow In dgvAgregados.Rows
                    '    If txtDescuento.Text <> 0 Then

                    '        If cboxPrecio.SelectedIndex = 0 Then 'minorista
                    '            t = t + (row.Cells("Precio Min.").Value - ((row.Cells("Precio Min.").Value * txtDescuento.Text) / 100)) * row.Cells("Cantidad").Value
                    '        ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta
                    '            t = t + (row.Cells("Precio Tarj.").Value - ((row.Cells("Precio Tarj.").Value * txtDescuento.Text) / 100)) * row.Cells("Cantidad").Value
                    '        ElseIf cboxPrecio.SelectedIndex = 2 Then 'mayorista
                    '            t = t + (row.Cells("Precio May.").Value - ((row.Cells("Precio May.").Value * txtDescuento.Text) / 100)) * row.Cells("Cantidad").Value
                    '        ElseIf cboxPrecio.SelectedIndex = 3 Then 'cuenta corriente
                    '            t = t + (row.Cells("Precio Min.").Value - ((row.Cells("Precio Min.").Value * txtDescuento.Text) / 100)) * row.Cells("Cantidad").Value
                    '        ElseIf cboxPrecio.SelectedIndex = 4 Then 'mercadopago
                    '            t = t + (row.Cells("Precio Min.").Value - ((row.Cells("Precio Min.").Value * txtDescuento.Text) / 100)) * row.Cells("Cantidad").Value
                    '        End If

                    '    Else

                    '        If cboxPrecio.SelectedIndex = 0 Then 'minorista
                    '            t = t + row.Cells("Precio Min.").Value * row.Cells("Cantidad").Value
                    '        ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta
                    '            t = t + row.Cells("Precio Tarj.").Value * row.Cells("Cantidad").Value
                    '        ElseIf cboxPrecio.SelectedIndex = 2 Then 'mayorista
                    '            t = t + row.Cells("Precio May.").Value * row.Cells("Cantidad").Value
                    '        ElseIf cboxPrecio.SelectedIndex = 3 Then 'cuenta corriente
                    '            t = t + row.Cells("Precio Min.").Value * row.Cells("Cantidad").Value
                    '        ElseIf cboxPrecio.SelectedIndex = 4 Then 'mercadopago
                    '            t = t + row.Cells("Precio Min.").Value * row.Cells("Cantidad").Value
                    '        End If

                    '    End If
                    'Next

                    ' Total
                    datosventas.Total_ = totalconDesc

                    ' Descuento
                    datosventas.Descuento_ = descuento

                    'Vendedor
                    If cboxVendedor.SelectedIndex <> -1 Then
                        datosventas.IdVendedor_ = cboxVendedor.SelectedValue
                    Else
                        datosventas.IdVendedor_ = Nothing
                    End If


                    ' Crear venta
                    datosventas.IdUsuario_ = 1
                    Dim idventa As Long
                    idventa = datosventas.InsertaVentas

                    ' Si la venta es con cuenta corriente
                    If cboxPrecio.SelectedIndex = 4 Or cboxPrecio.SelectedIndex = 5 Then

                        ' Verificar existencia de cuenta corriente
                        Dim verificar As New DatosCuentasCorrientes
                        Dim dtCuentaCorriente As New DataTable
                        verificar.IdCliente_ = idcliente
                        dtCuentaCorriente = verificar.VerificarCuenta

                        If dtCuentaCorriente.Rows.Count > 0 Then
                            ' Existe
                            Dim idCuenta As Long = dtCuentaCorriente.Rows(0)("IdCuenta")

                            ' Crear detalle
                            Dim detalle As New DatosCuentasCorrientes
                            detalle.Fecha_ = Date.Now
                            detalle.Monto_ = totalconDesc
                            detalle.Tipo_ = "Compra"
                            detalle.IdCuenta_ = idCuenta
                            detalle.IdVenta_ = idventa
                            detalle.CrearDetalle()
                            detalle.IncrementarSaldo()

                        Else
                            ' No existe
                            Dim cuenta As New DatosCuentasCorrientes
                            ' Controlar que haya un id cargado
                            cuenta.IdCliente_ = idcliente
                            cuenta.Saldo_ = totalconDesc
                            Dim idCuenta As Long
                            idCuenta = cuenta.CrearCuenta()

                            ' Crear detalle
                            Dim detalle As New DatosCuentasCorrientes
                            detalle.Fecha_ = Date.Now
                            detalle.Monto_ = totalconDesc
                            detalle.Tipo_ = "Compra"
                            detalle.IdCuenta_ = idCuenta
                            detalle.IdVenta_ = idventa
                            detalle.CrearDetalle()

                        End If
                    End If
#End Region

#Region "Crear Detalles"
                    For Each row As DataGridViewRow In dgvAgregados.Rows

                        Dim detalle As New DatosVentas

                        detalle.IdProducto_ = row.Cells("ID").Value
                        detalle.Descripcion_ = row.Cells("Descripción").Value
                        detalle.Marca_ = row.Cells("Marca").Value
                        detalle.Color_ = row.Cells("Color").Value
                        detalle.Talle_ = row.Cells("Talle").Value
                        detalle.Cantidad_ = row.Cells("Cantidad").Value

                        If cboxPrecio.SelectedIndex = 0 Then 'minorista
                            detalle.PrecioUnitario_ = row.Cells("Precio Min.").Value
                        ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta
                            detalle.PrecioUnitario_ = row.Cells("Precio Tarj.").Value
                        ElseIf cboxPrecio.SelectedIndex = 2 Then 'debito
                            detalle.PrecioUnitario_ = row.Cells("Precio Min.").Value
                        ElseIf cboxPrecio.SelectedIndex = 3 Then 'mayorista
                            detalle.PrecioUnitario_ = row.Cells("Precio May.").Value
                        ElseIf cboxPrecio.SelectedIndex = 4 Then 'cuenta corriente minorista
                            detalle.PrecioUnitario_ = row.Cells("Precio Min.").Value
                        ElseIf cboxPrecio.SelectedIndex = 5 Then 'cuenta corriente mayorista
                            detalle.PrecioUnitario_ = row.Cells("Precio May.").Value
                        ElseIf cboxPrecio.SelectedIndex = 6 Then 'mercadopago
                            detalle.PrecioUnitario_ = row.Cells("Precio Min.").Value
                        End If

                        detalle.Subtotal_ = detalle.PrecioUnitario_ * detalle.Cantidad_
                        detalle.IdVentas_ = idventa

                        detalle.CrearDetalleVenta()

                        ' disminuir cantidad stock
                        datosind.Cantidad_ = row.Cells("Cantidad").Value
                        datosind.IdProducto_ = row.Cells("ID").Value
                        datosind.DisminuirProducto()

                    Next
#End Region

                    ' Abrir ventana de facturación
                    Dim f As New Facturación("Finalizar venta", idventa, param_documentocliente, totalconDesc, descuento, tipopago, nombrecliente, direccioncliente)

                    f.Login = FormPrincipal.l
                    If f.ShowDialog() = DialogResult.OK Then

                        ' Exito
                        dgvAgregados.Rows.Clear()
                        idcliente = Nothing
                        documentocliente = 0
                        labelCliente.Text = "Seleccione un cliente"
                        txtCodVenta.Clear()
                        txtDescVenta.Clear()
                        txtTalle.Clear()
                        txtDescuento.Text = 0
                        txtTotal.Text = 0
                        NumCantVenta.Maximum = 1000
                        NumCantVenta.Value = 1
                        txtColorVenta.Clear()
                        txtPrecioVenta.Text = "Min."
                        txtPrecioTarj.Text = "Tarj."
                        txtPrecioMay.Text = "May."

                        bttElegirCliente.Text = "Elegir"
                        LabelTotal.Text = "Total: $ 0"
                        labelDescuento.Text = "Descuento: $ 0"
                        bttFinalizar.Enabled = False
                        bttEliminar.Enabled = False
                        totalmin = 0
                        totalmay = 0
                        totaltarj = 0
                        total = 0
                        totalconDesc = 0
                        descuento = 0
                        cantidad = 0
                        cboxPrecio.SelectedIndex = 0
                        cboxVendedor.SelectedIndex = -1
                        txtCodVenta.Enabled = True
                        txtCodVenta.Focus()

                    End If

                Else
                    'venta por cuenta corriente pero no hay un cliente seleccionado

                    MessageBox.Show("Debe seleccionar un cliente para continuar con una venta por cuenta corriente.")
                    Return

                End If

            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al finalizar la venta. Detalle: " & ex.Message.ToString)
            End Try


        End If
    End Sub


    Private Sub NuevaVenta_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        dgvAgregados.ColumnCount = 11

        dgvAgregados.Columns(0).Name = "Id"
        dgvAgregados.Columns(1).Name = "Código"
        dgvAgregados.Columns(2).Name = "Producto"
        dgvAgregados.Columns(3).Name = "Marca"
        dgvAgregados.Columns(4).Name = "Descripción"
        dgvAgregados.Columns(5).Name = "Talle"
        dgvAgregados.Columns(6).Name = "Color"
        dgvAgregados.Columns(7).Name = "Cantidad"
        dgvAgregados.Columns(7).HeaderText = "Cant."
        dgvAgregados.Columns(8).Name = "Precio Min."
        dgvAgregados.Columns(9).Name = "Precio May."
        dgvAgregados.Columns(10).Name = "Precio Tarj."

        dgvAgregados.Columns(2).FillWeight = 62
        dgvAgregados.Columns(7).FillWeight = 8
        dgvAgregados.Columns(8).FillWeight = 10
        dgvAgregados.Columns(9).FillWeight = 10
        dgvAgregados.Columns(10).FillWeight = 10

        dgvAgregados.Columns(0).Visible = False
        dgvAgregados.Columns(1).Visible = False
        dgvAgregados.Columns(3).Visible = False
        dgvAgregados.Columns(4).Visible = False
        dgvAgregados.Columns(5).Visible = False
        dgvAgregados.Columns(6).Visible = False

        dgvAgregados.ClearSelection()
        cboxPrecio.SelectedIndex = 0
        txtCodVenta.Focus()

    End Sub

    Private Sub txtDescuento_KeyDown(sender As Object, e As KeyEventArgs) Handles txtDescuento.KeyDown

        If e.KeyCode = Keys.Enter Then
            If txtDescuento.Text <> Trim("") Then ' Si hay descuento

                If txtDescuento.Text > 100 Then
                    txtDescuento.Text = 100
                    descuento = total
                    totalconDesc = 0
                    txtTotal.Text = totalconDesc
                    LabelTotal.Text = "Total: $ " & totalconDesc
                    labelDescuento.Text = "Descuento: $ " & descuento
                Else

                    If txtDescuento.Text <> 0 Then ' Hay descuento

                        totalconDesc = total - (txtDescuento.Text * total) / 100
                        descuento = (total * txtDescuento.Text) / 100

                    Else ' No hay descuento

                        totalconDesc = total
                        descuento = 0

                    End If

                    txtTotal.Text = totalconDesc
                    LabelTotal.Text = "Total: $ " & totalconDesc
                    labelDescuento.Text = "Descuento: $ " & descuento

                End If

            Else ' Si no hay descuento

                txtDescuento.Text = 0
                descuento = 0
                totalconDesc = total
                txtTotal.Text = totalconDesc
                LabelTotal.Text = "Total: $ " & totalconDesc
                labelDescuento.Text = "Descuento: $ " & descuento
                txtDescuento.SelectAll()

            End If
        End If

    End Sub

    Private Sub txtTotal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtTotal.KeyDown

        If e.KeyCode = Keys.Enter Then
            total = txtTotal.Text
            totalconDesc = total
            descuento = 0
            txtDescuento.Text = 0
            labelDescuento.Text = "Descuento: $ " & descuento
            LabelTotal.Text = "Total: $ " & totalconDesc
        End If

    End Sub

    Private Sub bttModificarProducto_Click(sender As Object, e As EventArgs) Handles bttModificarProducto.Click

        Dim f As New ModificarProducto(idprod_buscar, txtCodVenta.Text, "M")

        If f.ShowDialog() = DialogResult.OK Then

            idprod_buscar = 0

            txtCodVenta.Text = f.txtCod.Text

            Cargar_Datos()

        End If

    End Sub

    Private Sub bttAgregarProducto_Click(sender As Object, e As EventArgs) Handles bttAgregarProducto.Click

        Dim f As New ModificarProducto(idprod_buscar, txtCodVenta.Text, "A")

        If f.ShowDialog() = DialogResult.OK Then

            idprod_buscar = 0

            txtCodVenta.Text = f.txtCod.Text

            Cargar_Datos()

        End If

    End Sub

    Private Sub NuevaVenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim vendedor As New DatosVendedores
        cboxVendedor.DataSource = vendedor.ObtenerTodo
        cboxVendedor.DisplayMember = "Nombre"
        cboxVendedor.ValueMember = "IdVendedor"

        cboxVendedor.SelectedIndex = -1
    End Sub
End Class