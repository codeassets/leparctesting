USE [Leparc]
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[Documento] [bigint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Direccion] [varchar](50) NULL,
	[Telefono] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[DirImagen] [varchar](250) NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CuentasCorrientes]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CuentasCorrientes](
	[IdCuenta] [bigint] IDENTITY(1,1) NOT NULL,
	[IdCliente] [bigint] NULL,
	[Saldo] [decimal](15, 2) NULL,
 CONSTRAINT [PK_CuentasCorrientes] PRIMARY KEY CLUSTERED 
(
	[IdCuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetallesCuentas]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetallesCuentas](
	[IdDetalle] [bigint] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NULL,
	[Monto] [decimal](15, 2) NULL,
	[Tipo] [varchar](50) NULL,
	[IdCuenta] [bigint] NULL,
	[IdVenta] [bigint] NULL,
 CONSTRAINT [PK_DetallesCuentas] PRIMARY KEY CLUSTERED 
(
	[IdDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetallesFacturas]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetallesFacturas](
	[IdDetalle] [bigint] IDENTITY(1,1) NOT NULL,
	[IdProducto] [bigint] NULL,
	[Descripcion] [varchar](50) NULL,
	[Marca] [varchar](50) NULL,
	[Color] [varchar](50) NULL,
	[Talle] [varchar](50) NULL,
	[Cantidad] [int] NULL,
	[PrecioUnitario] [decimal](15, 2) NULL,
	[Subtotal] [decimal](15, 2) NULL,
	[IdFactura] [bigint] NULL,
 CONSTRAINT [PK_DetallesFacturas] PRIMARY KEY CLUSTERED 
(
	[IdDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetallesVentas]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetallesVentas](
	[IdDetalle] [bigint] IDENTITY(1,1) NOT NULL,
	[IdProducto] [bigint] NULL,
	[Descripcion] [varchar](50) NULL,
	[Marca] [varchar](50) NULL,
	[Color] [varchar](50) NULL,
	[Talle] [varchar](50) NULL,
	[Cantidad] [int] NULL,
	[PrecioUnitario] [decimal](15, 2) NULL,
	[Subtotal] [decimal](15, 2) NULL,
	[IdVenta] [bigint] NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_DetallesVentas] PRIMARY KEY CLUSTERED 
(
	[IdDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Facturas]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Facturas](
	[IdFactura] [bigint] IDENTITY(1,1) NOT NULL,
	[IdVenta] [bigint] NULL,
	[PtoVta] [nchar](10) NULL,
	[CbteTipo] [int] NULL,
	[CbteTipo_desc] [varchar](50) NULL,
	[Concepto] [int] NULL,
	[Concepto_desc] [varchar](50) NULL,
	[DocTipo] [int] NULL,
	[DocTipo_desc] [varchar](50) NULL,
	[DocNro] [bigint] NULL,
	[CbteDesde] [int] NULL,
	[CbteHasta] [nchar](10) NULL,
	[CbteFch] [datetime] NULL,
	[ImpTotal] [decimal](12, 2) NULL,
	[ImpTotConc] [decimal](12, 2) NULL,
	[ImpNeto] [decimal](12, 2) NULL,
	[ImpOpEx] [decimal](12, 2) NULL,
	[ImpTrib] [decimal](12, 2) NULL,
	[ImpIVA] [decimal](12, 2) NULL,
	[MonId] [varchar](50) NULL,
	[MonCotiz] [int] NULL,
	[AlicIva_Id] [int] NULL,
	[AlicIva_BaseImp] [decimal](12, 2) NULL,
	[AlicIva_Importe] [decimal](12, 2) NULL,
	[Estado] [varchar](50) NULL,
	[TipoPago] [varchar](50) NULL,
	[CAE] [varchar](20) NULL,
	[CAE_vto] [varchar](20) NULL,
	[Mensaje] [varchar](500) NULL,
 CONSTRAINT [PK_Facturas] PRIMARY KEY CLUSTERED 
(
	[IdFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Marcas]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Marcas](
	[IdMarca] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Marcas] PRIMARY KEY CLUSTERED 
(
	[IdMarca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Productos](
	[IdProducto] [bigint] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](50) NULL,
	[Marca] [varchar](50) NULL,
	[Descripcion] [varchar](50) NULL,
	[Talle] [varchar](10) NULL,
	[Color] [int] NULL,
	[NombreColor] [varchar](30) NULL,
	[PrecioMinorista] [decimal](10, 2) NULL,
	[PrecioMayorista] [decimal](10, 2) NULL,
	[PrecioTarjeta] [decimal](10, 2) NULL,
	[Cantidad] [nchar](10) NULL,
	[Fecha_A] [datetime] NULL,
	[Fecha_M] [datetime] NULL,
	[IdUsuario_A] [bigint] NULL,
	[IdUsuario_M] [bigint] NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Indumentaria] PRIMARY KEY CLUSTERED 
(
	[IdProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Talles]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Talles](
	[IdTalle] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Talles] PRIMARY KEY CLUSTERED 
(
	[IdTalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketsAcceso]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketsAcceso](
	[IdTA] [bigint] IDENTITY(1,1) NOT NULL,
	[Source] [varchar](100) NULL,
	[Destination] [varchar](100) NULL,
	[GenerationTime] [datetime] NULL,
	[ExpirationTime] [datetime] NULL,
	[Token] [varchar](1000) NULL,
	[Sign] [varchar](1000) NULL,
 CONSTRAINT [PK_TicketsAcceso] PRIMARY KEY CLUSTERED 
(
	[IdTA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ventas]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ventas](
	[IdVenta] [int] IDENTITY(1,1) NOT NULL,
	[IdCliente] [int] NOT NULL,
	[IdProducto] [int] NOT NULL,
	[Fecha] [date] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Importe] [decimal](15, 2) NOT NULL,
	[Tipo] [varchar](10) NULL,
	[Pago] [varchar](10) NULL,
	[Descuento] [int] NOT NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Ventas] PRIMARY KEY CLUSTERED 
(
	[IdVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ventas2]    Script Date: 16/05/2020 18:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ventas2](
	[IdVenta] [bigint] IDENTITY(1,1) NOT NULL,
	[IdCliente] [bigint] NULL,
	[Fecha] [date] NULL,
	[Tipo] [varchar](50) NULL,
	[Pago] [varchar](50) NULL,
	[Descuento] [decimal](15, 2) NULL,
	[Total] [decimal](15, 2) NULL,
	[IdUsuario] [bigint] NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Ventas2] PRIMARY KEY CLUSTERED 
(
	[IdVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Clientes] ADD  CONSTRAINT [DF_ActivoClientes]  DEFAULT ((1)) FOR [Activo]
GO
ALTER TABLE [dbo].[DetallesVentas] ADD  CONSTRAINT [DF_DetallesVentas_Activo]  DEFAULT ((1)) FOR [Activo]
GO
ALTER TABLE [dbo].[Marcas] ADD  CONSTRAINT [DF_ActivoMarcas]  DEFAULT ((1)) FOR [Activo]
GO
ALTER TABLE [dbo].[Productos] ADD  CONSTRAINT [DF_Activo]  DEFAULT ((1)) FOR [Activo]
GO
ALTER TABLE [dbo].[Talles] ADD  CONSTRAINT [DF_Talles_Activo]  DEFAULT ((1)) FOR [Activo]
GO
ALTER TABLE [dbo].[Ventas] ADD  CONSTRAINT [DF_Ventas_Descuento]  DEFAULT ((0)) FOR [Descuento]
GO
ALTER TABLE [dbo].[Ventas] ADD  CONSTRAINT [DF_ActivoVentas]  DEFAULT ((1)) FOR [Activo]
GO
ALTER TABLE [dbo].[Ventas2] ADD  CONSTRAINT [DF_Ventas2_Activo]  DEFAULT ((1)) FOR [Activo]
GO
USE [master]
GO
ALTER DATABASE [Leparc] SET  READ_WRITE 
GO
